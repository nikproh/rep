package app;


public enum EntityState {
    
    approved,
    discard,
    undefined;
    
    public static EntityState stateByString(String stateString) {
        
        switch (stateString) {
        case "approved":
            return EntityState.approved;
        case "discard":
            return EntityState.discard;
        case "undefined":
            return EntityState.undefined;
        }
        
        return null;
    }
}