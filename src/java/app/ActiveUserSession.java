package app;


public class ActiveUserSession {
    Long id = null;
    String token = "";
    Long dateValid = null;
    ActiveUser activeUser = null;

    public void setId(Long id) {
        this.id = id;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public void setDateValid(Long dateValid) {
        this.dateValid = dateValid;
    }

    public void setActiveUser(ActiveUser activeUser) {
        this.activeUser = activeUser;
    }

    public Long getId() {
        return id;
    }

    public String getToken() {
        return token;
    }

    public Long getDateValid() {
        return dateValid;
    }

    public ActiveUser getActiveUser() {
        return activeUser;
    }
}
