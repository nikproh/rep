package app;


public class UserRole {    
    Long id = null;
    String roleName = null;
    String userName = null;

    public UserRole() {
    }
    
    public UserRole(ActiveUser user, String roleNameIn) {
        userName = user.login;
        roleName = roleNameIn;
    }

    public Long getId() {
        return id;
    }

    public String getRoleName() {
        return roleName;
    }

    public String getUserName() {
        return userName;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public void setRoleName(String roleName) {
        this.roleName = roleName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }    
}
