package app;


import java.util.Date;


public class TemporaryUser {
    Long id = null;
    String login = "";
    String password = "";
    //String email = "";
    String confirmationString = "";
    String name = "";
    Long dateValid = null;
    
    public TemporaryUser() {        
    }
    
    public TemporaryUser(String loginIn, String passwordIn, String nameIn, Long dateValidIn, String confirmationStringIn) {      
        login = loginIn;
        password = passwordIn;
        //email = emailIn;
        name = nameIn;
        dateValid = dateValidIn;
        confirmationString = confirmationStringIn;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public void setConfirmationString(String confirmationString) {
        this.confirmationString = confirmationString;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setDateValid(Long dateValid) {
        this.dateValid = dateValid;
    }

    public Long getId() {
        return id;
    }

    public String getLogin() {
        return login;
    }

    public String getPassword() {
        return password;
    }

    public String getConfirmationString() {
        return confirmationString;
    }

    public String getName() {
        return name;
    }

    public Long getDateValid() {
        return dateValid;
    }   
}
