package app;


public class ActiveUser {
    Long id = null;
    String login = "";
    String password = "";
    //String email = "";
    String name = "";
    Long dateCreation = null;    
    UserRole role = null;
    Boolean ifDisable = false;
    
    public ActiveUser() {        
    }
    
    /*
    public User(String loginIn, String passwordIn, String emailIn, String confirmationStringIn) {      
        login = loginIn;
        password = passwordIn;
        email = emailIn;
    }*/
    
    public ActiveUser(TemporaryUser temporaryUser) {      
        login = temporaryUser.getLogin();
        password = temporaryUser.getPassword();
        //email = temporaryUser.getEmail();
        name = temporaryUser.name;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setDateCreation(Long dateCreation) {
        this.dateCreation = dateCreation;
    }

    public void setRole(UserRole role) {
        this.role = role;
    }

    public void setIfDisable(Boolean ifDisable) {
        this.ifDisable = ifDisable;
    }    

    public Long getId() {
        return id;
    }

    public String getLogin() {
        return login;
    }

    public String getPassword() {
        return password;
    }

    public String getName() {
        return name;
    }

    public Long getDateCreation() {
        return dateCreation;
    }   

    public UserRole getRole() {
        return role;
    }

    public Boolean getIfDisable() {
        return ifDisable;
    }
}
