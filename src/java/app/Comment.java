package app;


public class Comment {
    Long id = null;
    String text = "";
    Long date = null;
    String dateStr = "";
    ActiveUser activeUser = null;
    Question question = null;
    EntityState state = null;  

    public void setId(Long id) {
        this.id = id;
    }

    public void setText(String text) {
        this.text = text;
    }

    public void setDate(Long date) {
        this.date = date;
    }

    public void setActiveUser(ActiveUser activeUser) {
        this.activeUser = activeUser;
    }

    public void setQuestion(Question question) {
        this.question = question;
    }

    public void setDateStr(String dateStr) {
        this.dateStr = dateStr;
    }    

    public void setState(EntityState state) {
        this.state = state;
    }      
       
    public String getText() {
        return text;
    }

    public Long getDate() {
        return date;
    }

    public ActiveUser getActiveUser() {
        return activeUser;
    }

    public Long getId() {
        return id;
    }

    public Question getQuestion() {
        return question;
    }

    public String getDateStr() {
        return dateStr;
    }

    public EntityState getState() {
        return state;
    }  
}