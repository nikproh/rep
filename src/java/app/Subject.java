package app;


import java.util.Set;


public class Subject {
    Long id = null;
    String name = "";
    EntityState state = null;
    Set<Question> questions = null;
    Long dateCreation = null;
    Boolean ifGroup = false;
    
    public Subject() {
    }

    public Subject(String nameIn) {        
        name = nameIn;
    }
    
    public Subject(Long idIn, String nameIn) {
        id = idIn;
        name = nameIn;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setState(EntityState state) {
        this.state = state;
    }

    public void setQuestions(Set<Question> questions) {
        this.questions = questions;
    }

    public void setDateCreation(Long dateCreation) {
        this.dateCreation = dateCreation;
    }

    public void setIfGroup(Boolean ifGroup) {
        this.ifGroup = ifGroup;
    }   

    public Long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public EntityState getState() {
        return state;
    }

    public Set<Question> getQuestions() {
        return questions;
    }

    public Long getDateCreation() {
        return dateCreation;
    }

    public Boolean getIfGroup() {
        return ifGroup;
    }
}