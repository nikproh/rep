package app;


import java.util.Set;


public class Question {
    Long id = null;
    String text = "";
    //String body = "";
    ActiveUser activeUser = null;
    Set<Comment> comments = null;
    Set<Subject> subjects = null;
    Long dateCreation = null;   
    Long dateStateSetting = null;       
    EntityState state = null;       
            
    public Question() {        
    }    
    
    public Question(Long idIn, String textIn) {
        id = idIn;
        text = textIn;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public void setText(String text) {
        this.text = text;
    }

    public void setActiveUser(ActiveUser activeUser) {
        this.activeUser = activeUser;
    }

    public void setComments(Set<Comment> comments) {
        this.comments = comments;
    }

    public void setSubjects(Set<Subject> subjects) {
        this.subjects = subjects;
    }    

    public void setDateCreation(Long dateCreation) {
        this.dateCreation = dateCreation;
    }

    public void setState(EntityState state) {
        this.state = state;
    }

    /*public void setBody(String body) {
        this.body = body;
    }*/

    public void setDateStateSetting(Long dateStateSetting) {
        this.dateStateSetting = dateStateSetting;
    }

    public Long getId() {
        return id;
    }

    public String getText() {
        return text;
    }

    public ActiveUser getActiveUser() {
        return activeUser;
    }

    public Set<Comment> getComments() {
        return comments;
    }

    public Set<Subject> getSubjects() {
        return subjects;
    }   

    public Long getDateCreation() {
        return dateCreation;
    }

    public EntityState getState() {
        return state;
    }

    /*public String getBody() {
        return body;
    } */   

    public Long getDateStateSetting() {
        return dateStateSetting;
    }
}
