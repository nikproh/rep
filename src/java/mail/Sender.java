package mail;


import java.util.Properties;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;


public class Sender {
    
    /*
    public static boolean sendMail1(String massege, String recepient) {
        
        Properties props = new Properties();
        Session session = Session.getDefaultInstance(props, null);

        try {
            Message msg = new MimeMessage(session);
            msg.setFrom(new InternetAddress("admin@test.ru", "Example.com Admin"));
            msg.addRecipient(Message.RecipientType.TO,
                             new InternetAddress(recepient, "Mr. User"));
            msg.setSubject("Your Example.com account has been activated");
            msg.setText(massege);
            
            Transport.send(msg);            
        } catch (Exception ex) {
            ex.printStackTrace();
            return false;
        }
        
        return true;
    }*/
    
    public static boolean sendMail(String body, String subject, String recepient) {

            final String username = "nikproh.nikproh@gmail.com";
            final String password = "##";

            Properties props = new Properties();
            props.put("mail.smtp.auth", "true");
            props.put("mail.smtp.starttls.enable", "true");
            props.put("mail.smtp.host", "smtp.gmail.com");
            props.put("mail.smtp.port", "587");

            Session session = Session.getInstance(props,
              new javax.mail.Authenticator() {
                    protected PasswordAuthentication getPasswordAuthentication() {
                            return new PasswordAuthentication(username, password);
                    }
              });

            try {

                    Message message = new MimeMessage(session);
                    message.setFrom(new InternetAddress("nikproh.nikproh@gmail.com"));
                    message.setRecipients(Message.RecipientType.TO,
                            InternetAddress.parse(recepient));
                    message.setSubject(subject);
                    message.setText(body);

                    Transport.send(message);

                    //System.out.println("Done");

            } catch (MessagingException ex) {
                    ex.printStackTrace();
                    return false;
            }
            return true;
	}
}
