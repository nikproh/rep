package security;


import java.security.Principal;
import javax.servlet.http.HttpServletRequest;


public class PageAccess {

    public static String dispatcherByRoles(HttpServletRequest request, String ... validRoles) {
                
        String result = "/errorPages/notLogin.jsp";
        
        Principal user = request.getUserPrincipal();        
        if (user != null) {
        
            boolean ifInRole = false;
            for (String role : validRoles) {
                if (request.isUserInRole(role)) {
                    result = null;
                    ifInRole = true;
                    break;
                }
            }        
            
            if (ifInRole == false) {
                result = "/errorPages/notAuthorize.jsp";
            }
        }
        
        return result;
    }  
    
    /*
    public static String dispatcherByLogins(HttpServletRequest request, String ... validLogins) {
                
        String result = "/errorPages/notLogin.jsp";
        
        Principal user = request.getUserPrincipal();        
        if (user != null) {
        
            boolean ifInRole = false;
            for (String login : validLogins) {
                if (user.equals(login)) {
                    result = null;
                    ifInRole = true;
                    break;
                }
            }        
            
            if (ifInRole == false) {
                result = "/errorPages/notAuthorize.jsp";
            }
        }
        
        return result;
    } */ 
}
