package servlet;


import app.ActiveUser;
import db.DAO;
import java.io.IOException;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import mail.Sender;
import security.PageAccess;
import stringgenerator.Password;
import utils.Popup;


public class PasswordReset extends HttpServlet { 
           
    @Override
    public void doPost(HttpServletRequest request, 
            HttpServletResponse response) 
            throws ServletException, IOException {
        doGet(request, response);
    }
    
    @Override
    public void doGet(HttpServletRequest request, 
            HttpServletResponse response) 
            throws ServletException, IOException {    
                
        String dispatch = PageAccess.dispatcherByRoles(request, "admin", "user");        
        if (dispatch == null) {
            
            dispatch = "/errorPages/notLogout.jsp";
        } else {
        
            dispatch = "/passwordResetForm";    
            String login = request.getParameter("login");           
                                     
            try {           
                DAO dao = new DAO();   
                ActiveUser activeUser = dao.getActiveUserByName(login);
                dao.close();
                
                if (activeUser != null) {
                    
                    boolean sendMail = PasswordResetClass.proceed(activeUser);
                    if (sendMail) {
                        dispatch = "/loginForm"; 
                        new Popup(true, true, "Password was succesfully reset. Check your email.").setInRequest(request);                        
                    } else {
                        new Popup(true, false, "Fail while send mail").setInRequest(request);
                    }                    
                } else {
                    new Popup(true, false, "There is no such user").setInRequest(request);
                }
            } catch (Exception ex) {
                ex.printStackTrace();
                new Popup(true, false, "Some error while password reset").setInRequest(request);     
            }            
        }
        
        RequestDispatcher view = request.getRequestDispatcher(dispatch);
        view.forward(request, response);
    }  
}
