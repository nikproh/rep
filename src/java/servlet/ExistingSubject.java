package servlet;


import app.Question;
import app.Subject;
import db.DAO;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import utils.Popup;
import utils.QuestionsById;


public class ExistingSubject extends HttpServlet { 
 
    @Override
    public void doPost(HttpServletRequest request, 
            HttpServletResponse response) 
            throws ServletException, IOException {
        doGet(request, response);
    }
    
    @Override
    public void doGet(HttpServletRequest request, 
            HttpServletResponse response) 
            throws ServletException, IOException {                       
        
        String dispatch = "/allSubjects";
        
        String pathInfo = request.getPathInfo();
        String subjectId = pathInfo.substring(1);
            
        request.setAttribute("subjectId", subjectId);
        
        try {     
            
            DAO dao = new DAO();   
            
            Subject subject = null;            
            if (request.isUserInRole("admin")) {            
                subject = dao.getSubjectByNumberString(subjectId);
            } else {
                subject = dao.getSubjectByNumberStringWithApprovedQuestion(subjectId);
            }            
            
            if (subject != null) {
                request.setAttribute("subject", subject);  
                
                ArrayList<Question> subjectQuestions = new ArrayList<>(subject.getQuestions());                
                Collections.sort(subjectQuestions, new QuestionsById());
                
                request.setAttribute("questionsList", subjectQuestions);
                request.setAttribute("questionsListSize", subjectQuestions.size());
                dispatch = "/pages/subject.jsp";            
            } else {                
                new Popup(true, false, "There is no such subject").setInRequest(request);     
            }
            dao.close();
        } catch (Exception ex) {
            
            ex.printStackTrace();
            request.setAttribute("subject", null);  
            request.setAttribute("questionsList", null);
            new Popup(true, false, "Error while read subject").setInRequest(request);            
        }     
        
        RequestDispatcher view = request.getRequestDispatcher(dispatch);      
        view.forward(request, response);
    }                                                                               
}

