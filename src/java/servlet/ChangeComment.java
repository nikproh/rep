package servlet;


import db.DAO;
import java.io.IOException;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import security.PageAccess;
import utils.Popup;


public class ChangeComment extends HttpServlet { 
 
    @Override
    public void doPost(HttpServletRequest request, 
            HttpServletResponse response) 
            throws ServletException, IOException {  
        
        doGet(request, response); 
    }
    
    @Override
    public void doGet(HttpServletRequest request, 
            HttpServletResponse response) 
            throws ServletException, IOException {                      
        
        String dispatch = PageAccess.dispatcherByRoles(request, "admin");        
        if (dispatch == null) {                      
            String questioncString = request.getParameter("questionId");             
            dispatch = "/question/" + questioncString;   
            
            String pathInfo = request.getPathInfo();
            String commentNumberString = pathInfo.substring(1);  
            
            try {     
                String commentText = request.getParameter("commentText"); 
            
                DAO dao = new DAO();            
                boolean result = dao.setCommentText(commentNumberString, commentText);                
                if (!result) {
                    new Popup(true, false, "Error while comment changing").setInRequest(request);                    
                } 
                
                request.setAttribute("commentShow", true);    
                
                dao.close();           

            } catch (Exception ex) {
                new Popup(true, false, "Error while comment changing").setInRequest(request);  
                ex.printStackTrace();
            }  
        }
        
        RequestDispatcher view = request.getRequestDispatcher(dispatch);      
        view.forward(request, response);
    }                                                                               
}