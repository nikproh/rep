package servlet;


import app.ActiveUser;
import db.DAO;
import java.io.IOException;
import java.security.Principal;
import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import utils.UrlHandler;


public class AutoLoginFilter implements Filter {
    
    @Override
    public void init(FilterConfig filterConfig) throws ServletException {
    }
    
    @Override
    public void doFilter(ServletRequest request, ServletResponse response, 
                         FilterChain chain)                
      throws ServletException, IOException {  
        
        HttpServletRequest httpRequest = (HttpServletRequest)request;
        
        //String currentPage = (String)request.getParamert("currentPage");  
        //String currentPage = httpRequest.getRequestURI();
        //String referrer = httpRequest.getHeader("referer");
        
        //String currentPageUri = UrlHandler.getCurrentPageUrl(httpRequest);
        String currentPagePath = UrlHandler.getCurrentPagePath(httpRequest);
        
        //request.setAttribute("currentPageUri", currentPageUri);
        request.setAttribute("referer", currentPagePath);
        
        Principal user = httpRequest.getUserPrincipal();        
        if (user == null) {        
            Cookie[] cookies = httpRequest.getCookies(); 

            if (cookies != null) {
                String userToken = "";
                for (Cookie cookie : cookies) {
                    String cookieName = cookie.getName();    
                    if (cookieName.equals("userToken")) {
                        userToken = cookie.getValue();
                    }
                }

                if (!userToken.equals("")) {
                    try {
                        
                        DAO dao = new DAO();  
                        ActiveUser activeUser = dao.getTokenUser(userToken);
                        dao.close();
                        
                        HttpServletResponse httpResponse = (HttpServletResponse)response;
                        if (activeUser != null) {
                            httpRequest.login(activeUser.getLogin(), activeUser.getPassword());  
                            httpRequest.authenticate(httpResponse);
                        } else {
                            Cookie usernameCookie = new Cookie("userToken", "");
                            usernameCookie.setMaxAge(0); 
                            httpResponse.addCookie(usernameCookie);
                        }                        
                    } catch (Exception ex) {
                        ex.printStackTrace();
                    }
                }
            }
        }
        
        /*
        Principal user = httpRequest.getUserPrincipal();        
        if (user == null) {        
            Cookie[] cookies = httpRequest.getCookies(); 

            if (cookies != null) {
                String username = "";
                String password = "";
                for (Cookie cookie : cookies) {
                    String cookieName = cookie.getName();    
                    switch (cookieName) {
                    case "username":
                        username = cookie.getValue();
                        break;
                    case "password":
                        password = cookie.getValue();
                        break;
                    }
                }

                if ((!username.equals("")) && (!password.equals(""))) {
                    try {
                        
                        DAO dao = new DAO();  
                        boolean result = dao.checkActiveUserIfDisable(username);
                        dao.close();
                        
                        HttpServletResponse httpResponse = (HttpServletResponse)response;
                        if (!result) {
                            httpRequest.login(username, password);  
                            httpRequest.authenticate(httpResponse);
                        } else {
                            Cookie usernameCookie = new Cookie("username", "");
                            usernameCookie.setMaxAge(0); 
                            httpResponse.addCookie(usernameCookie);
                            Cookie passwordCookie = new Cookie("password", "");
                            passwordCookie.setMaxAge(0); 
                            httpResponse.addCookie(passwordCookie);
                        }                        
                    } catch (Exception ex) {
                        ex.printStackTrace();
                    }
                }
            }
        }*/
        
        chain.doFilter(request, response);                                        
    }
    
    @Override
    public void destroy() {          
    }
}