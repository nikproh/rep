package servlet;


import app.ActiveUser;
import app.Question;
import app.Subject;
import db.DAO;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import security.PageAccess;
import utils.Popup;
import utils.QuestionsById;


public class UserQuestions extends HttpServlet {        
   
    @Override
    public void doPost(HttpServletRequest request, 
            HttpServletResponse response) 
            throws ServletException, IOException {
        doGet(request, response);
    }
    
    @Override
    public void doGet(HttpServletRequest request, 
            HttpServletResponse response) 
            throws ServletException, IOException {  
        
        String dispatch = PageAccess.dispatcherByRoles(request, "admin", "user"); 
                    
        if (dispatch == null) {
            
            dispatch = "/pages/userQuestions.jsp";
            
            try {    
                DAO dao = new DAO();                            
                ActiveUser activeUser = dao.getActiveUserByName(request.getRemoteUser());                
                ArrayList<Question> userQuestions = dao.getUserQuestions(activeUser);     
                Subject subject = dao.getSubjectByName("All questions");
                dao.close();

                Collections.sort(userQuestions, new QuestionsById());
                Collections.reverse(userQuestions);
                
                for (Question question : userQuestions) {
                    ArrayList<Subject> subjects = new ArrayList<>();
                    for (Subject subjectCicle : question.getSubjects()) {
                        if (!subjectCicle.getName().equals("All questions")) {
                            subjects.add(subjectCicle);  
                        }
                    }
                    question.setSubjects(new HashSet(subjects));
                }

                request.setAttribute("questionsList", userQuestions);
                request.setAttribute("questionsListSize", userQuestions.size());
                request.setAttribute("subject", subject);

            } catch (Exception ex) {            
                ex.printStackTrace();
                request.setAttribute("questionsList", null);
                new Popup(true, false, "Error while reading questions").setInRequest(request);            
            }     
        }
        
        RequestDispatcher view = request.getRequestDispatcher(dispatch);      
        view.forward(request, response);
    }                                                                               
}
