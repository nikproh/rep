package servlet;


import app.ActiveUser;
import db.DAO;
import mail.Sender;
import stringgenerator.Password;
import static utils.MD5Generator.md5Custom;


public class PasswordResetClass {

    public static boolean proceed(ActiveUser activeUser) {
        
        try {
            String newPassword = Password.nextPassword(8);
            newPassword = "phoebeCOOL001";      
            String newPasswordHash = md5Custom(newPassword);
            
            DAO dao = new DAO();
            dao.changePassword(activeUser, newPasswordHash);
            String sendMessage = "You new password: " + newPassword;
            boolean sendMailResult = Sender.sendMail(sendMessage, "Password reset", activeUser.getLogin());
            dao.close();
            return sendMailResult;            
        } catch (Exception ex) {
            ex.printStackTrace();
            return false;
        }
    }
}
