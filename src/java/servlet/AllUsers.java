package servlet;


import app.ActiveUser;
import app.UserRole;
import db.DAO;
import java.io.IOException;
import java.util.ArrayList;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import utils.Popup;


public class AllUsers extends HttpServlet {        
   
    @Override
    public void doPost(HttpServletRequest request, 
            HttpServletResponse response) 
            throws ServletException, IOException {
        doGet(request, response);
    }
    
    @Override
    public void doGet(HttpServletRequest request, 
            HttpServletResponse response) 
            throws ServletException, IOException {                       
                
        try {     
            
            DAO dao = new DAO();            
            ArrayList<ActiveUser> allUsers = dao.getAllUsers();    
            dao.getUsersRoles(allUsers);  
            dao.close();
            
            request.setAttribute("allUsers", allUsers);
            request.setAttribute("allUsersSize", allUsers.size());
                                                        
        } catch (Exception ex) {            
            ex.printStackTrace();            
            request.setAttribute("allUsers", null);
            new Popup(true, false, "Error while reading users").setInRequest(request); 
        }     
        
        RequestDispatcher view = request.getRequestDispatcher("/pages/allUsers.jsp");      
        view.forward(request, response);
    }                                                                               
}
