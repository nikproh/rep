package servlet;


import app.ActiveUser;
import db.DAO;
import java.io.IOException;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import mail.Sender;
import security.PageAccess;
import utils.Popup;


public class PasswordChange extends HttpServlet { 
           
    @Override
    public void doPost(HttpServletRequest request, 
            HttpServletResponse response) 
            throws ServletException, IOException {
        doGet(request, response);
    }
    
    @Override
    public void doGet(HttpServletRequest request, 
            HttpServletResponse response) 
            throws ServletException, IOException {    
                
        String dispatch = PageAccess.dispatcherByRoles(request, "admin", "user");  
        
        if (dispatch == null) {
            dispatch = "/passwordChangeForm";    
            String oldPassword = request.getParameter("oldPassword");
            String newPassword = request.getParameter("newPassword");
            
            DAO dao = new DAO();                
            try {                
                ActiveUser activeUser = dao.getActiveUserByName(request.getRemoteUser());
                String g = activeUser.getPassword();
                if (activeUser.getPassword().equals(oldPassword)) {
                    boolean changeResult = dao.changePassword(activeUser, newPassword);
                    if (changeResult) {
                        //String sendMessage = "You password was shange to: " + newPassword;
                        String sendMessage = "You password was change.";
                        boolean sendMail = Sender.sendMail(sendMessage, "Change password", request.getRemoteUser());
                        dispatch = "/"; 
                        new Popup(true, true, "Password was successfully change").setInRequest(request);                        
                    } else {
                        new Popup(true, false, "Some error while password change").setInRequest(request);
                    }
                } else {
                    new Popup(true, false, "Old password is wrong").setInRequest(request);
                }
            } catch (Exception ex) {
                ex.printStackTrace();
                new Popup(true, false, "Some error while password change").setInRequest(request);                
            }
            dao.close();            
        }
        
        RequestDispatcher view = request.getRequestDispatcher(dispatch);
        view.forward(request, response);
    }
}
