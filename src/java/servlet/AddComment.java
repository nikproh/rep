package servlet;


import app.Comment;
import app.EntityState;
import db.DAO;
import java.io.IOException;
import java.util.Date;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import security.PageAccess;
import utils.Popup;


public class AddComment extends HttpServlet { 
     
    @Override
    public void doPost(HttpServletRequest request, 
            HttpServletResponse response) 
            throws ServletException, IOException {
        doGet(request, response);
    }
    
    @Override
    public void doGet(HttpServletRequest request, 
            HttpServletResponse response) 
            throws ServletException, IOException {                       
        
        String dispatch = PageAccess.dispatcherByRoles(request, "admin", "user");        
        if (dispatch == null) {
            
            String questionNumberString = request.getParameter("questionId").trim();
            
            dispatch = "/question/" + questionNumberString;    
            request.setAttribute("commentShow", true);
        
            String commentText = request.getParameter("commentText");
            
            try {     

                DAO dao = new DAO();
                
                Comment comment = new Comment();
                                
                comment.setText(commentText);
                comment.setQuestion(dao.getQuestionByNumberString(questionNumberString));                
                comment.setActiveUser(dao.getActiveUserByName(request.getRemoteUser()));
                comment.setDate(new Date().getTime());
                
                boolean ifAdmin = request.isUserInRole("admin");
                
                if (ifAdmin) {
                    comment.setState(EntityState.approved);
                } else {
                    comment.setState(EntityState.undefined);
                }
                
                boolean result = dao.addComment(comment);
                if (!result) {
                    new Popup(true, false, "Error while adding comment").setInRequest(request);                    
                } 
                dao.close();         

            } catch (Exception ex) {
                new Popup(true, false, "Error while adding comment").setInRequest(request);  
                ex.printStackTrace();
            }     
        } 
        
        RequestDispatcher view = request.getRequestDispatcher(dispatch);      
        view.forward(request, response);
    }                                                                               
}
