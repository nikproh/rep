package servlet;


import app.ActiveUser;
import app.UserRole;
import db.DAO;
import java.io.IOException;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import security.PageAccess;
import utils.Popup;
import utils.UrlHandler;


public class UserChange extends HttpServlet { 
     
    @Override
    public void doPost(HttpServletRequest request, 
            HttpServletResponse response) 
            throws ServletException, IOException {
        doGet(request, response);
    }
    
    @Override
    public void doGet(HttpServletRequest request, 
            HttpServletResponse response) 
            throws ServletException, IOException {                      
        
        String dispatch = PageAccess.dispatcherByRoles(request, "admin");
        
        if (dispatch == null) {
            
            dispatch = "/allUsers";
            String activeUserId = request.getParameter("userId");            
        
            DAO dao = new DAO();
            ActiveUser activeUser = dao.getActiveUserByNumberString(activeUserId);
            
            if (activeUser != null) {
                
                try {               
                    
                    String passwordReset = request.getParameter("passwordReset");  
                    String userDisable = request.getParameter("userDisable");
                    String roles = request.getParameter("roles");
                    
                    if (passwordReset.equals("true")) {
                        
                        boolean sendMail = PasswordResetClass.proceed(activeUser);
                        
                        if (sendMail) { 
                            new Popup(true, true, "Password was succesfully reset. Email was send.").setInRequest(request);                        
                        } else {
                            new Popup(true, false, "Fail while send mail").setInRequest(request);
                        }           
                    } if (userDisable.equals("true")) {
                        
                        boolean result = dao.userDisable(activeUser);
                        if (result) { 
                            new Popup(true, true, "User state was succesfully change.").setInRequest(request);                        
                        } else {
                            new Popup(true, false, "Fail while user state change").setInRequest(request);
                        }                        
                    } if (roles.equals("true")) {
                        
                        boolean result = dao.changeUserRole(activeUser);
                        
                        if (result == true) {
                            new Popup(true, true, "Role was successfully change").setInRequest(request);
                        } else {
                            new Popup(true, false, "Same error while role change").setInRequest(request);
                        }
                    }
                    
                } catch (Exception ex) {

                    ex.printStackTrace();  
                    new Popup(true, false, "Same error while user change").setInRequest(request);
                }   

                dao.close();
                
            } else {
                new Popup(true, false, "There is no such user").setInRequest(request); 
            }
        } 
        
        request.setAttribute("setPageUrl", UrlHandler.getUrl(request, dispatch));
        RequestDispatcher view = request.getRequestDispatcher(dispatch);      
        view.forward(request, response);
    }                                                                               
}