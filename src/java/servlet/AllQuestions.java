package servlet;


import app.Question;
import db.DAO;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import utils.Popup;
import utils.QuestionsById;


public class AllQuestions extends HttpServlet {        
   
    @Override
    public void doPost(HttpServletRequest request, 
            HttpServletResponse response) 
            throws ServletException, IOException {
        doGet(request, response);
    }
    
    @Override
    public void doGet(HttpServletRequest request, 
            HttpServletResponse response) 
            throws ServletException, IOException {   
        
        try {    
            
            DAO dao = new DAO();            
            ArrayList<Question> allQuestions = dao.getAllApprovedQuestions();                        
            dao.close();
            
            Collections.sort(allQuestions, new QuestionsById());

            request.setAttribute("questionsList", allQuestions);
            request.setAttribute("questionsListSize", allQuestions.size());
            
        } catch (Exception ex) {            
            ex.printStackTrace();
            request.setAttribute("questionsList", null);
            new Popup(true, false, "Error while reading questions").setInRequest(request);            
        }     
        
        RequestDispatcher view = request.getRequestDispatcher("/pages/allQuestions.jsp");      
        view.forward(request, response);
    }                                                                               
}
