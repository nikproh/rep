package servlet;


import app.ActiveUser;
import app.Subject;
import db.DAO;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import security.PageAccess;
import utils.Popup;
import utils.SubjectsByDateSort;


public class UserSubjects extends HttpServlet {        
   
    @Override
    public void doPost(HttpServletRequest request, 
            HttpServletResponse response) 
            throws ServletException, IOException {
        doGet(request, response);
    }
    
    @Override
    public void doGet(HttpServletRequest request, 
            HttpServletResponse response) 
            throws ServletException, IOException {  
        
        String dispatch = PageAccess.dispatcherByRoles(request, "admin", "user"); 
                    
        if (dispatch == null) {
            
            dispatch = "/pages/userSubjects.jsp";
            
            try {    
                DAO dao = new DAO();                            
                ActiveUser activeUser = dao.getActiveUserByName(request.getRemoteUser());                
                ArrayList<Subject> userSubjects = dao.getUserSubjects(activeUser);                        
                dao.close();

                Collections.sort(userSubjects, new SubjectsByDateSort());

                request.setAttribute("userSubjects", userSubjects);
                request.setAttribute("userSubjectsSize", userSubjects.size());

            } catch (Exception ex) {            
                ex.printStackTrace();
                request.setAttribute("userSubjects", null);
                new Popup(true, false, "Error while reading subjects").setInRequest(request);            
            }     
        }
        
        RequestDispatcher view = request.getRequestDispatcher(dispatch);      
        view.forward(request, response);
    }                                                                               
}
