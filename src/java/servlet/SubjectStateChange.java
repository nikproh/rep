package servlet;


import app.EntityState;
import app.Subject;
import db.DAO;
import java.io.IOException;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import security.PageAccess;
import utils.Popup;
import utils.UrlHandler;


public class SubjectStateChange extends HttpServlet { 
     
    @Override
    public void doPost(HttpServletRequest request, 
            HttpServletResponse response) 
            throws ServletException, IOException {
        doGet(request, response);
    }
    
    @Override
    public void doGet(HttpServletRequest request, 
            HttpServletResponse response) 
            throws ServletException, IOException {                      
        
        String dispatch = PageAccess.dispatcherByRoles(request, "admin");
        
        if (dispatch == null) {
            
            String subjectId = request.getParameter("subjectId");            
        
            DAO dao = new DAO();
            Subject subject = dao.getSubjectByNumberString(subjectId);
            
            if (subject != null) {
                
                try {               
                    
                    String state = request.getParameter("newState");                                
                    subject.setState(EntityState.valueOf(state));
                    dao.addSubject(subject);   
                    
                    //dispatch = "/subject/" + subjectNumber;
                    dispatch = "/" + request.getParameter("oldState") + "Subjects";
                    
                } catch (Exception ex) {

                    ex.printStackTrace();                    
                    
                    dispatch = "/allSubjects";
                    new Popup(true, false, "Same error while subject state change").setInRequest(request);
                }   

                dao.close();
                
            } else {
                new Popup(true, false, "There is no such subject").setInRequest(request); 
                dispatch = "/allSubjects"; 
            }
        } 
        
        request.setAttribute("setPageUrl", UrlHandler.getUrl(request, dispatch));
        RequestDispatcher view = request.getRequestDispatcher(dispatch);      
        view.forward(request, response);
    }                                                                               
}