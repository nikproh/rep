package servlet;


import db.DAO;
import javax.servlet.http.HttpServlet;


public class InitServlet extends HttpServlet { 
        
    @Override
    public void init() {
        DAO.init();
    }
}
