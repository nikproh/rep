package servlet;


import app.EntityState;
import app.Question;
import app.Subject;
import db.DAO;
import java.io.IOException;
import java.util.ArrayList;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import utils.Popup;


public class AllSubjects extends HttpServlet {        
   
    @Override
    public void doPost(HttpServletRequest request, 
            HttpServletResponse response) 
            throws ServletException, IOException {
        doGet(request, response);
    }
    
    @Override
    public void doGet(HttpServletRequest request, 
            HttpServletResponse response) 
            throws ServletException, IOException {                       
                
        try {     
            
            DAO dao = new DAO();            
            ArrayList<Subject> approvedSubjects = dao.getAllApprovedSubjects();                        
            dao.close();
            
            ArrayList<Subject> allSubjects = new ArrayList<>();
            ArrayList<Integer> allSubjectsLen = new ArrayList<>();
            ArrayList<Subject> allGroups = new ArrayList<>();
            ArrayList<Integer> allGroupsLen = new ArrayList<>();
            
            for (Subject subject : approvedSubjects) {
                
                int count = 0;
                for (Question question : subject.getQuestions()) {
                    if (question.getState() == EntityState.approved) {
                        count++;
                    }
                }
                
                if (subject.getIfGroup() == false) {
                    allSubjects.add(subject);
                    allSubjectsLen.add(count);
                } else {
                    allGroups.add(subject);
                    allGroupsLen.add(count);
                }
            }
            
            request.setAttribute("allSubjects", allSubjects);
            request.setAttribute("allSubjectsLen", allSubjectsLen);
            request.setAttribute("allSubjectsSize", allSubjects.size());
            request.setAttribute("allGroups", allGroups);
            request.setAttribute("allGroupsLen", allGroupsLen);
            request.setAttribute("allGroupsSize", allGroups.size());
                                                        
        } catch (Exception ex) {            
            ex.printStackTrace();            
            request.setAttribute("allSubjects", null);            
            new Popup(true, false, "Error while reading subjects").setInRequest(request); 
        }     
        
        RequestDispatcher view = request.getRequestDispatcher("/pages/allSubjects.jsp");      
        view.forward(request, response);
    }                                                                               
}
