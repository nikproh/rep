package servlet;


import app.Subject;
import db.DAO;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import utils.Popup;
import utils.SubjectsByDateSort;


public class DiscardSubjects extends HttpServlet {        
   
    @Override
    public void doPost(HttpServletRequest request, 
            HttpServletResponse response) 
            throws ServletException, IOException {
        doGet(request, response);
    }
    
    @Override
    public void doGet(HttpServletRequest request, 
            HttpServletResponse response) 
            throws ServletException, IOException {   
        
        request.setAttribute("subjectType", "discard");
        
        try {    
            
            DAO dao = new DAO();            
            ArrayList<Subject> discardSubjects = dao.getAllDiscardSubjects();                        
            dao.close();
            
            Collections.sort(discardSubjects, new SubjectsByDateSort());
            
            request.setAttribute("subjects", discardSubjects);
            request.setAttribute("subjectsSize", discardSubjects.size());
            
        } catch (Exception ex) {            
            ex.printStackTrace();
            request.setAttribute("subjects", null);
            new Popup(true, false, "Error while reading subjects").setInRequest(request);            
        }     
        
        RequestDispatcher view = request.getRequestDispatcher("/pages/discardSubjects.jsp");      
        view.forward(request, response);
    }                                                                               
}
