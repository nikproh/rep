package servlet;


import app.Question;
import java.io.IOException;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import db.DAO;
import java.util.ArrayList;
import java.util.Collections;
import utils.Popup;
import utils.QuestionsById;


public class ApprovedQuestions extends HttpServlet {        
   
    @Override
    public void doPost(HttpServletRequest request, 
            HttpServletResponse response) 
            throws ServletException, IOException {
        doGet(request, response);
    }
    
    @Override
    public void doGet(HttpServletRequest request, 
            HttpServletResponse response) 
            throws ServletException, IOException {   
        
        try {    
            
            DAO dao = new DAO();            
            ArrayList<Question> questionsList = dao.getAllApprovedQuestions();                        
            dao.close();
            
            Collections.sort(questionsList, new QuestionsById());
            Collections.reverse(questionsList);
            
            request.setAttribute("questionsList", questionsList);
            request.setAttribute("questionsListSize", questionsList.size());
            
        } catch (Exception ex) {            
            ex.printStackTrace();
            request.setAttribute("questionsList", null);
            new Popup(true, false, "Error while reading questions").setInRequest(request);            
        }     
        
        RequestDispatcher view = request.getRequestDispatcher("/pages/approvedQuestions.jsp");      
        view.forward(request, response);
    }                                                                               
}
