package servlet;


import java.io.IOException;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import security.PageAccess;


public class AddSubjectForm extends HttpServlet { 
    
    @Override
    public void doPost(HttpServletRequest request, 
            HttpServletResponse response) 
            throws ServletException, IOException {
        doGet(request, response);
    }
    
    @Override
    public void doGet(HttpServletRequest request, 
            HttpServletResponse response) 
            throws ServletException, IOException {                      
        
        String dispatch = PageAccess.dispatcherByRoles(request, "admin", "user");        
        if (dispatch == null) {            
            dispatch = "/pages/addSubjectForm.jsp"; 
            
            if (request.isUserInRole("admin")) {            
                request.setAttribute("ifAdmin", true); 
            } else {
                request.setAttribute("ifAdmin", false); 
            }
        } 
        
        RequestDispatcher view = request.getRequestDispatcher(dispatch);      
        view.forward(request, response);
    }                                                                               
}
