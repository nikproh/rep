package servlet;


import app.TemporaryUser;
import app.ActiveUser;
import db.DAO;
import java.io.IOException;
import java.util.Date;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import utils.Popup;


public class RegistrationConfirmation extends HttpServlet { 
        
    @Override
    public void doPost(HttpServletRequest request, 
            HttpServletResponse response) 
            throws ServletException, IOException {
        doGet(request, response);
    }
    
    @Override
    public void doGet(HttpServletRequest request, 
            HttpServletResponse response) 
            throws ServletException, IOException {    
                    
        String dispatch = "/registrationForm";
        try {     
            
            String confirmationString = request.getParameter("confirmationString");
            
            DAO dao = new DAO();            
            TemporaryUser temporaryUser = dao.getTemporaryUserByConfirmationString(confirmationString);                       
            if (temporaryUser != null) {
                
                if (dao.ifActiveUserExistByName(temporaryUser.getLogin())) {
                    
                    new Popup(true, false, "User already exist").setInRequest(request);                    
                    
                } else {
                    ActiveUser user = new ActiveUser(temporaryUser);    
                    user.setDateCreation(new Date().getTime());
                    boolean addUserResult = dao.addUser(user);
                    if (addUserResult) {
                        dispatch = "/loginForm";
                        new Popup(true, true, "Registration complite - you can now login").setInRequest(request);
                        request.setAttribute("username", user.getLogin());
                    } else {
                        new Popup(true, false, "Some error while user adding").setInRequest(request);                        
                    }                    
                }
            } else {
                new Popup(true, false, "Wrong confirmation string").setInRequest(request);
            }
            dao.close();
                                                
        } catch (Exception ex) {                  
            ex.printStackTrace();     
            new Popup(true, false, "Some error while user adding").setInRequest(request);
        }   
            
        RequestDispatcher view = request.getRequestDispatcher(dispatch);
        view.forward(request, response);
    }                                                                               
}

