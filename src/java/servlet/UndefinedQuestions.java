package servlet;


import app.Question;
import db.DAO;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import security.PageAccess;
import utils.Popup;
import utils.QuestionsById;


public class UndefinedQuestions extends HttpServlet {        
   
    @Override
    public void doPost(HttpServletRequest request, 
            HttpServletResponse response) 
            throws ServletException, IOException {
        doGet(request, response);
    }
    
    @Override
    public void doGet(HttpServletRequest request, 
            HttpServletResponse response) 
            throws ServletException, IOException {  
        
        String dispatch = PageAccess.dispatcherByRoles(request, "admin"); 
                    
        if (dispatch == null) {
            
            dispatch = "/pages/undefinedQuestions.jsp";
            
            try {    
                DAO dao = new DAO();            
                ArrayList<Question> questionsList = dao.getAllUndefinedQuestions();                        
                dao.close();

                Collections.sort(questionsList, new QuestionsById());
                Collections.reverse(questionsList);
                
                request.setAttribute("questionsList", questionsList);
                request.setAttribute("questionsListSize", questionsList.size());

            } catch (Exception ex) {            
                ex.printStackTrace();
                request.setAttribute("questionsList", null);
                new Popup(true, false, "Error while reading questions").setInRequest(request);            
            }     
        }
        
        RequestDispatcher view = request.getRequestDispatcher(dispatch);      
        view.forward(request, response);
    }                                                                               
}
