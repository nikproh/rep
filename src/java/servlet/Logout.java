package servlet;


import java.io.IOException;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import utils.Popup;
import utils.UrlHandler;


public class Logout extends HttpServlet { 
           
    @Override
    public void doPost(HttpServletRequest request, 
            HttpServletResponse response) 
            throws ServletException, IOException {
        doGet(request, response);
    }
    
    @Override
    public void doGet(HttpServletRequest request, 
            HttpServletResponse response) 
            throws ServletException, IOException {     

        /*
        String currentPageUrl = (String)request.getAttribute("currentPageUrl");        
        String currentPagePath = (String)request.getAttribute("currentPagePath");        
        */
        
        String dispatch = "/allSubjects";
        String referer = (String)request.getAttribute("referer");
        if (referer != null) {
            dispatch = referer;
        }
        
        try {                

            request.logout();
            request.getSession().invalidate();
            
            Cookie tokenCookie = new Cookie("userToken", "");
            tokenCookie.setMaxAge(0); 
            response.addCookie(tokenCookie);
            
            /*
            Cookie usernameCookie = new Cookie("username", "");
            usernameCookie.setMaxAge(0); 
            response.addCookie(usernameCookie);
            Cookie passwordCookie = new Cookie("password", "");
            passwordCookie.setMaxAge(0); 
            response.addCookie(passwordCookie);
            */
                    
        } catch (Exception ex) {    
            
            new Popup(true, false, "Error while logout").setInRequest(request); 
            ex.printStackTrace();            
        }   
                
        request.setAttribute("setPageUrl", UrlHandler.getUrl(request, dispatch));
        RequestDispatcher view = request.getRequestDispatcher(dispatch);
        //request.setAttribute("setPageUrl", currentPageUrl);        
        //RequestDispatcher view = request.getRequestDispatcher(currentPagePath);
        view.forward(request, response);
    } 
}
