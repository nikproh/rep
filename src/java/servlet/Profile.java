package servlet;


import app.ActiveUser;
import app.EntityState;
import app.Subject;
import db.DAO;
import java.io.IOException;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import security.PageAccess;
import utils.Popup;
import utils.UrlHandler;


public class Profile extends HttpServlet { 
     
    @Override
    public void doPost(HttpServletRequest request, 
            HttpServletResponse response) 
            throws ServletException, IOException {
        doGet(request, response);
    }
    
    @Override
    public void doGet(HttpServletRequest request, 
            HttpServletResponse response) 
            throws ServletException, IOException {                      
        
        String dispatch = PageAccess.dispatcherByRoles(request, "admin", "user");
        
        if (dispatch == null) {
                    
            DAO dao = new DAO();
            ActiveUser activeUser = dao.getActiveUserByName(request.getRemoteUser());
            
            if (activeUser != null) {
                
                try {               
                    dispatch = "/pages/profile.jsp"; 
                    request.setAttribute("user", activeUser);
                    
                } catch (Exception ex) {

                    ex.printStackTrace();                    
                    
                    dispatch = "/start"; 
                    new Popup(true, false, "Same error while get user").setInRequest(request);
                }   

                dao.close();
                
            } else {
                new Popup(true, false, "There is no such user").setInRequest(request); 
                dispatch = "/start"; 
            }
        } 
                
        RequestDispatcher view = request.getRequestDispatcher(dispatch);      
        view.forward(request, response);
    }                                                                               
}