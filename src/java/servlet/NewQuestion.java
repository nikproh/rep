package servlet;


import app.Question;
import app.Subject;
import db.DAO;
import java.io.IOException;
import java.util.ArrayList;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import security.PageAccess;


public class NewQuestion extends HttpServlet { 
           
    @Override
    public void doPost(HttpServletRequest request, 
            HttpServletResponse response) 
            throws ServletException, IOException {
        doGet(request, response);
    }
    
    @Override
    public void doGet(HttpServletRequest request, 
            HttpServletResponse response) 
            throws ServletException, IOException {    
                
        String dispatch = PageAccess.dispatcherByRoles(request, "admin", "user");  
        
        if (dispatch == null) {
            dispatch = "/pages/question.jsp";    
            
            Object question = request.getAttribute("question");
            if (question == null) {
                request.setAttribute("question", new Question());
            }            
            request.setAttribute("ifNew", true);
            
            DAO dao = new DAO();
            
            ArrayList<Subject> allApprovedSubjects = dao.getAllApprovedSubjectsForList();  
            
            request.setAttribute("allSubjects", allApprovedSubjects); 
            dao.close();
        }
        
        RequestDispatcher view = request.getRequestDispatcher(dispatch);
        view.forward(request, response);
    }
}
