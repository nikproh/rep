package servlet;


import app.EntityState;
import app.Question;
import app.Subject;
import db.DAO;
import java.io.IOException;
import java.util.Date;
import java.util.HashSet;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import security.PageAccess;
import utils.Popup;
import utils.UrlHandler;


public class SaveQuestion extends HttpServlet { 
     
    @Override
    public void doPost(HttpServletRequest request, 
            HttpServletResponse response) 
            throws ServletException, IOException {
        doGet(request, response);
    }
    
    @Override
    public void doGet(HttpServletRequest request, 
            HttpServletResponse response) 
            throws ServletException, IOException {                      
        
        request.setCharacterEncoding("UTF-8");
        
        String dispatch = ""; 
        Question question = null;
        
        String questionNumber = request.getParameter("questionNumber");
        if (questionNumber.equals("new")) {
            dispatch = PageAccess.dispatcherByRoles(request, "admin", "user"); 
            question = new Question();
            question.setDateCreation(new Date().getTime());
        } else {
            dispatch = PageAccess.dispatcherByRoles(request, "admin");
            DAO dao = new DAO();
            question = dao.getQuestionByNumberString(questionNumber);
            dao.close();
        }
        
        if (dispatch == null) {
            
            boolean ifAdmin = request.isUserInRole("admin");
            
            if (question != null) {
                
                try {     
                    
                    DAO dao = new DAO();
                    
                    String questionText = request.getParameter("questionText");            
                    question.setText(questionText);       
                    if (questionNumber.equals("new")) {
                        question.setActiveUser(dao.getActiveUserByName(request.getRemoteUser()));
                    }
                    String subjectsString = request.getParameter("subjects");
                    String[] subjects = new String[0];
                    
                    if (!subjectsString.equals("")) {
                        subjects = subjectsString.split(",");
                    }
                          
                    question.setSubjects(new HashSet<>());                    
                    
                    if (subjects != null) {                       
                                        
                        for (String subjectId : subjects) {
                            Subject subject = dao.getSubjectByNumberString(subjectId);
                            if (subject != null) {
                                question.getSubjects().add(subject);
                            }
                        }
                    }
                                        
                    String newSubjectName = request.getParameter("newSubject");
                    if (!newSubjectName.equals("")) {
                        Subject newSubject = dao.getSubjectByName(newSubjectName);
                        if (newSubject == null) {
                            Subject subject = new Subject(newSubjectName);
                            if (!ifAdmin) {
                                subject.setState(EntityState.undefined);
                            } else {
                                subject.setState(EntityState.approved);
                            }
                            boolean result = dao.addSubject(subject);
                            if (result) {
                                newSubject = dao.getSubjectByName(newSubjectName);                            
                            }
                        }
                        if (newSubject != null) {
                            question.getSubjects().add(newSubject);
                        }
                    }
                    dao.close();
                    
                    if (question.getSubjects().isEmpty()) {
                        request.setAttribute("question", question);  
                        dispatch = "/question/new";
                        new Popup(true, false, "No subject was choosen").setInRequest(request);
                    } else {
                    
                        DAO daoForQuestion = new DAO();
                        
                        Subject allQuestionSubjects = daoForQuestion.getSubjectByName("All questions");
                        question.getSubjects().add(allQuestionSubjects);
                        
                        if (!ifAdmin) {
                            question.setState(EntityState.undefined);                         

                            boolean result = daoForQuestion.addQuestion(question);
                            if (result) {
                                dispatch = "/userQuestions";
                                new Popup(true, true, "Your question would be considered").setInRequest(request);
                            } else {
                                request.setAttribute("question", question);  
                                dispatch = "/question/new";
                                new Popup(true, false, "Same error while question addition").setInRequest(request);
                            }

                        } else {                        

                            if (questionNumber.equals("new")) {
                                question.setState(EntityState.approved);                            
                                new Popup(true, true, "Question was successfully add").setInRequest(request);
                            } else {
                                String state = request.getParameter("state");   
                                if (!state.equals("")) {
                                    question.setState(EntityState.valueOf(state));
                                }
                                new Popup(true, true, "Question was successfully change").setInRequest(request);
                            } 

                            boolean result = daoForQuestion.addQuestion(question);
                            if (result) {
                                
                                if (questionNumber.equals("new")) {
                                    dispatch = "/question/" + allQuestionSubjects.getId() + "/" + question.getId();
                                } else {
                                
                                    String subject = request.getParameter("subject");
                                    long subjectId = Long.parseLong(subject, 10);

                                    boolean bool = false;
                                    for (Subject subjectInList : question.getSubjects()) {
                                        if (subjectInList.getId() == subjectId) {
                                            bool = true;
                                            break;
                                        }  
                                    }

                                    if (bool) {
                                        dispatch = "/question/" + subjectId + "/" + question.getId();
                                    } else {
                                        dispatch = "/question/" + allQuestionSubjects.getId() + "/" + question.getId();
                                    }       
                                }
                                
                            } else {
                                request.setAttribute("question", question);  
                                dispatch = "/question/new";
                                new Popup(true, false, "Same error while question addition").setInRequest(request);
                            }
                        }   
                        daoForQuestion.close();
                    }
                } catch (Exception ex) {

                    ex.printStackTrace();
                    
                    request.setAttribute("question", question);  
                    dispatch = "/question/new";
                    new Popup(true, false, "Same error while question addition").setInRequest(request);
                }   
                
            } else {
                new Popup(true, false, "There is no such question").setInRequest(request); 
                dispatch = "/question/new"; 
            }
        } 
        
        request.setAttribute("setPageUrl", UrlHandler.getUrl(request, dispatch));
        RequestDispatcher view = request.getRequestDispatcher(dispatch);      
        view.forward(request, response);
    }                                                                               
}