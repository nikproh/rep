package servlet;


import app.EntityState;
import app.Subject;
import db.DAO;
import java.io.IOException;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import security.PageAccess;
import utils.Popup;


public class AddSubject extends HttpServlet { 
     
    @Override
    public void doPost(HttpServletRequest request, 
            HttpServletResponse response) 
            throws ServletException, IOException {
        doGet(request, response);
    }
    
    @Override
    public void doGet(HttpServletRequest request, 
            HttpServletResponse response) 
            throws ServletException, IOException {                       
        
        String dispatch = PageAccess.dispatcherByRoles(request, "admin");        
        if (dispatch == null) {
            
            dispatch = "/allSubjects";        
        
            String subjectName = request.getParameter("subjectName");
            
            try {     

                DAO dao = new DAO();   
                Subject subject = new Subject(subjectName);
                subject.setState(EntityState.approved);
                boolean result = dao.addSubject(subject);
                if (!result) {
                    new Popup(true, false, "Error while adding subject").setInRequest(request);
                } else {
                    new Popup(true, true, "Subject was successfully add").setInRequest(request);
                }
                dao.close();                        
                
            } catch (Exception ex) {
                new Popup(true, false, "Error while adding subject").setInRequest(request);
                ex.printStackTrace();
            }     
        } 
        
        RequestDispatcher view = request.getRequestDispatcher(dispatch);      
        view.forward(request, response);
    }                                                                               
}
