package servlet;


import db.DAO;
import java.io.IOException;
import java.security.Principal;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import utils.Popup;
import utils.UrlHandler;


public class Login extends HttpServlet { 
           
    @Override
    public void doPost(HttpServletRequest request, 
            HttpServletResponse response) 
            throws ServletException, IOException {
        doGet(request, response);
    }
    
    @Override
    public void doGet(HttpServletRequest request, 
            HttpServletResponse response) 
            throws ServletException, IOException {     
                
        String dispatch = "/allSubjects";
        
        Principal user = request.getUserPrincipal();        
        if (user == null) {
            
            String username = request.getParameter("username");
            String password = request.getParameter("password");
            String ifRemember = request.getParameter("remember");
            
            try { 
                
                DAO dao = new DAO();  
                boolean result = dao.checkActiveUserIfDisable(username);
                
                if (!result) {
                    
                    request.login(username, password);  
                    request.authenticate(response);
                    //request.getSession().setMaxInactiveInterval(60*60*24*365);
                
                    if (ifRemember != null) {
                    
                        String token = dao.setActiveUserSessionToken(username);

                        if (token != null) {
                            Cookie tokenCookie = new Cookie("userToken", token);
                            tokenCookie.setMaxAge(60*60*24*30); 
                            response.addCookie(tokenCookie);
                        }
                    }
                    /*
                    Cookie usernameCookie = new Cookie("username", username);
                    usernameCookie.setMaxAge(60*60*24*365); 
                    response.addCookie(usernameCookie);
                    Cookie passwordCookie = new Cookie("password", password);
                    passwordCookie.setMaxAge(60*60*24*365); 
                    response.addCookie(passwordCookie);
                    */
                    
                    //String g = UrlHandler.getUrl(request, dispatch);

                    request.setAttribute("setPageUrl", UrlHandler.getUrl(request, dispatch));  
                } else {
                    dispatch = "/loginForm";
                    request.setAttribute("username", username);
                    new Popup(true, false, "Login is disable").setInRequest(request);   
                }
                
                dao.close();
            } catch (Exception ex) {  
                dispatch = "/loginForm";
                request.setAttribute("username", username);
                new Popup(true, false, "Error while login").setInRequest(request);                
                ex.printStackTrace();            
            }   
        }
         
        RequestDispatcher view = request.getRequestDispatcher(dispatch);
        view.forward(request, response);
    }     
}
