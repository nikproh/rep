package servlet;


import app.TemporaryUser;
import db.DAO;
import java.io.IOException;
import java.util.Date;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import stringgenerator.RandomString;
import mail.Sender;
import utils.Popup;


public class Registration extends HttpServlet { 
        
    @Override
    public void doPost(HttpServletRequest request, 
            HttpServletResponse response) 
            throws ServletException, IOException {
        doGet(request, response);
    }
    
    @Override
    public void doGet(HttpServletRequest request, 
            HttpServletResponse response) 
            throws ServletException, IOException {
        
        String dispatch = "/registrationForm";
        
        try {     
            
            String username = request.getParameter("username");
            String password = request.getParameter("password");
            String name = request.getParameter("name");
            request.setAttribute("username", username);
            request.setAttribute("name", name);

            DAO dao = new DAO();            
            
            if (dao.ifActiveUserExistByName(username) || dao.ifTemporaryUserExistByName(username)) {
                new Popup(true, false, "User already exist").setInRequest(request); 
            } else {
            
                RandomString randomString = new RandomString(20);
                String confirmationString = randomString.nextString();

                Date nowDate = new Date();
                Long validDate = nowDate.getTime() + 60 * 60 * 1000;            

                TemporaryUser temporaryUser = new TemporaryUser(username, password, name, validDate, confirmationString);            

                boolean resultAddTemporaryUser = dao.addTemporaryUser(temporaryUser);
                dao.close();

                if (resultAddTemporaryUser) {
                    String sendMessage = "Thanks for registration!\nYou login: " + username + "\nTo complite registration follow that link: \n http://jqsite.spbstu.ru/registrationConfirmation?confirmationString=" + confirmationString;
                    boolean sendMail = Sender.sendMail(sendMessage, "Registration", username);
                    if (sendMail) {
                        dispatch = "/start";
                        new Popup(true, true, "To continue registaration check you email").setInRequest(request);
                    } else {                        
                        new Popup(true, false, "Error while send mail").setInRequest(request);
                    }
                } else {
                    new Popup(true, false, "Some error while registration").setInRequest(request);
                }           
            }
            
        } catch (Exception ex) {    
            new Popup(true, false, "Some error while registration").setInRequest(request);
            ex.printStackTrace();            
        }   
        
        RequestDispatcher view = request.getRequestDispatcher(dispatch);
        view.forward(request, response);
    }                                                                               
}
