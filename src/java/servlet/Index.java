package servlet;


import java.io.IOException;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import utils.UrlHandler;


public class Index extends HttpServlet { 
           
    @Override
    public void doPost(HttpServletRequest request, 
            HttpServletResponse response) 
            throws ServletException, IOException {
        doGet(request, response);
    }
    
    @Override
    public void doGet(HttpServletRequest request, 
            HttpServletResponse response) 
            throws ServletException, IOException { 

        String dispatch = "/start";
        request.setAttribute("setPageUrl", UrlHandler.getUrl(request, dispatch));
        RequestDispatcher view = request.getRequestDispatcher(dispatch);
        view.forward(request, response);
    } 
}
