package servlet;


import app.Subject;
import db.DAO;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import utils.Popup;
import utils.SubjectsByDateSort;


public class UndefinedSubjects extends HttpServlet {        
   
    @Override
    public void doPost(HttpServletRequest request, 
            HttpServletResponse response) 
            throws ServletException, IOException {
        doGet(request, response);
    }
    
    @Override
    public void doGet(HttpServletRequest request, 
            HttpServletResponse response) 
            throws ServletException, IOException {   
        
        request.setAttribute("subjectType", "undefined");
        
        try {    
            
            DAO dao = new DAO();            
            ArrayList<Subject> undefinedSubjects = dao.getAllUndefinedSubjects();                        
            dao.close();
            
            Collections.sort(undefinedSubjects, new SubjectsByDateSort());
            
            request.setAttribute("subjects", undefinedSubjects);
            request.setAttribute("subjectsSize", undefinedSubjects.size());
            
        } catch (Exception ex) {            
            ex.printStackTrace();
            request.setAttribute("subjects", null);
            new Popup(true, false, "Error while reading subjects").setInRequest(request);            
        }     
        
        RequestDispatcher view = request.getRequestDispatcher("/pages/undefinedSubjects.jsp");      
        view.forward(request, response);
    }                                                                               
}
