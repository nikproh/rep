package servlet;


import db.DAO;
import java.io.IOException;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import security.PageAccess;


public class Header extends HttpServlet { 
           
    @Override
    public void doPost(HttpServletRequest request, 
            HttpServletResponse response) 
            throws ServletException, IOException {
        doGet(request, response);
    }
    
    @Override
    public void doGet(HttpServletRequest request, 
            HttpServletResponse response) 
            throws ServletException, IOException {  
        
        String dispatch = PageAccess.dispatcherByRoles(request, "admin", "user");
        if (dispatch == null) {
            request.setAttribute("ifAuthtorize", true);
        } else {
            request.setAttribute("ifAuthtorize", false);
        }
        
        if (request.isUserInRole("admin")) {            
            request.setAttribute("ifAdmin", true); 
        } else {
            request.setAttribute("ifAdmin", false); 
        }
        
        try {
            DAO dao = new DAO();            
            request.setAttribute("undefinedQuestionsSize", dao.getAllUndefinedQuestionsSize());                     
            dao.close();        
        } catch (Exception ex) {
            request.setAttribute("undefinedQuestionsSize", 0); 
        }
        
        try {
            DAO dao = new DAO();            
            request.setAttribute("undefinedSubjectsSize", dao.getAllUndefinedSubjectsSize());                     
            dao.close();        
        } catch (Exception ex) {
            request.setAttribute("undefinedSubjectsSize", 0); 
        }
        
        try {
            DAO dao = new DAO();            
            request.setAttribute("newComments", dao.getNewCommentsSize());                     
            dao.close();        
        } catch (Exception ex) {
            request.setAttribute("newComments", 0); 
        }
                
        RequestDispatcher view = request.getRequestDispatcher("/pages/header.jsp");              
        view.include(request, response);
    } 
}
