package servlet;


import app.Comment;
import app.EntityState;
import app.Question;
import app.Subject;
import java.io.IOException;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import db.DAO;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import security.PageAccess;
import utils.CommentByDateSort;
import utils.Popup;


public class ExistingQuestion extends HttpServlet { 
 
    @Override
    public void doPost(HttpServletRequest request, 
            HttpServletResponse response) 
            throws ServletException, IOException {
        doGet(request, response);
    }
    
    @Override
    public void doGet(HttpServletRequest request, 
            HttpServletResponse response) 
            throws ServletException, IOException {                       
        
        String dispatch = PageAccess.dispatcherByRoles(request, "admin", "user");        
        boolean ifAdmin = request.isUserInRole("admin");                
                
        if (dispatch == null) {
            request.setAttribute("ifAuthtorize", true);
            
            if (ifAdmin) {            
                request.setAttribute("ifAdmin", true); 
            } else {
                request.setAttribute("ifAdmin", false); 
            }
            
        } else {
            request.setAttribute("ifAuthtorize", false);
        }
        
        //String requestURI = request.getRequestURI();
        //String questionNumberString = requestURI.substring(requestURI.lastIndexOf('/') + 1);
        String pathInfo = request.getPathInfo();
        pathInfo = pathInfo.substring(1);
        
        String questionNumberString = "";
        String subjectNumberString = "";
        int slashPoshition = pathInfo.indexOf("/");
        if (slashPoshition != -1) {
            subjectNumberString = pathInfo.substring(0, slashPoshition); 
            questionNumberString = pathInfo.substring(slashPoshition + 1);
        } else {
            questionNumberString = pathInfo.substring(0);            
        }
            
        //request.setAttribute("questionNumberString", questionNumberString);
        
        try {     
            
            DAO dao = new DAO();            
            Question question = dao.getQuestionByNumberString(questionNumberString);
            
            if (question != null) {
                
                ArrayList<Comment> comments = new ArrayList<Comment>();
                
                if (ifAdmin) {
                    comments.addAll(question.getComments());
                } else {
                    for (Comment comment : question.getComments()) {
                        if (comment.getState() != EntityState.discard) {
                            comments.add(comment);
                        }
                    }
                }
                
                Collections.sort(comments, new CommentByDateSort());
                
                for (Comment comment : comments) {
                    
                    Date date = new Date(comment.getDate());                    
                    DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
                    String dateStr = dateFormat.format(date);

                    comment.setDateStr(dateStr);
                }
                
                request.setAttribute("comments", comments);
                request.setAttribute("commentsCount", comments.size());
                
                Subject subject = null;
                if (subjectNumberString.equals("")) {
                    subject = dao.getSubjectByName("All questions");
                } else {
                    subject = dao.getSubjectByNumberString(subjectNumberString);
                }
                request.setAttribute("subject", subject);
                
                request.setAttribute("question", question);           
                dispatch = "/pages/question.jsp";
                
                request.setAttribute("nextQuestionId", dao.getNextQuestionId(question, subject));
                request.setAttribute("previousQuestionId", dao.getPreviousQuestionId(question, subject));
                
                ArrayList<Subject> allApprovedSubjects = dao.getAllApprovedSubjectsForList();                
                
                request.setAttribute("allSubjects", allApprovedSubjects); 
            
            } else {
                dispatch = "/allSubjects";
                new Popup(true, false, "There is no such question").setInRequest(request);     
            }
            dao.close();
        } catch (Exception ex) {
            
            ex.printStackTrace();
            
            dispatch = "/allSubjects";
            new Popup(true, false, "Error while read question").setInRequest(request);            
        }     
        
        RequestDispatcher view = request.getRequestDispatcher(dispatch);      
        view.forward(request, response);
    }                                                                               
}

