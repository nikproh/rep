package servlet;


import app.Subject;
import db.DAO;
import java.io.IOException;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import security.PageAccess;
import utils.Popup;
import utils.UrlHandler;


public class SubjectToGroup extends HttpServlet { 
     
    @Override
    public void doPost(HttpServletRequest request, 
            HttpServletResponse response) 
            throws ServletException, IOException {
        doGet(request, response);
    }
    
    @Override
    public void doGet(HttpServletRequest request, 
            HttpServletResponse response) 
            throws ServletException, IOException {                      
        
        request.setCharacterEncoding("UTF-8");
        
        String dispatch = PageAccess.dispatcherByRoles(request, "admin");        
        
        if (dispatch == null) {
            
            String subjectNumber = request.getParameter("subjectId");
            
            try {  
                DAO dao = new DAO();

                Subject subject = dao.getSubjectByNumberString(subjectNumber);

                if (subject != null) {
                    Boolean subjectIfGroup = subject.getIfGroup();
                    subject.setIfGroup(!subjectIfGroup);
                    dao.addSubject(subject);
                    dispatch = "/" + request.getParameter("oldState") + "Subjects";
                } else {
                    new Popup(true, false, "There is no such subject").setInRequest(request); 
                    dispatch = "/allSubjects"; 
                }
            } catch (Exception ex) {

                ex.printStackTrace();
                dispatch = "/allSubjects";
                new Popup(true, false, "Same error while subject rename").setInRequest(request);
            } 
        } 
        
        request.setAttribute("setPageUrl", UrlHandler.getUrl(request, dispatch));
        RequestDispatcher view = request.getRequestDispatcher(dispatch);      
        view.forward(request, response);
    }                                                                               
}