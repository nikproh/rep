package stringgenerator;

    
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Random;


public class Password {
    
    private static final char[] symbols;
    private static final char[] numbers;
    private static final char[] spetialSymbols;

    static {
        StringBuilder tmp = new StringBuilder();
        for (char ch = 'a'; ch <= 'z'; ++ch)
            tmp.append(ch);
        for (char ch = 'A'; ch <= 'Z'; ++ch)
            tmp.append(ch);
        symbols = tmp.toString().toCharArray();
        
        tmp = new StringBuilder();
        for (char ch = '0'; ch <= '9'; ++ch)
            tmp.append(ch);
        numbers = tmp.toString().toCharArray();
        
        tmp = new StringBuilder();
        tmp.append('!');
        tmp.append('@');
        tmp.append('#');
        tmp.append('$');
        tmp.append('%');        
        tmp.append('&');
        tmp.append('*');
        spetialSymbols = tmp.toString().toCharArray();
    }

    public static String nextPassword(int length) {
        
        if (length < 7) {
            throw new IllegalArgumentException("length < 7: " + length);
        }
        
        String unShuffled = "";
        Random random = new Random(new Date().getTime());
        
        unShuffled += spetialSymbols[random.nextInt(spetialSymbols.length)];        
        
        unShuffled += numbers[random.nextInt(numbers.length)];
        unShuffled += numbers[random.nextInt(numbers.length)];
        
        for (int idx = 3; idx < length; idx++) 
            unShuffled += symbols[random.nextInt(symbols.length)];
        
        List<String> letters = Arrays.asList(unShuffled.split(""));
        Collections.shuffle(letters);
        String shuffled = "";
        for (String letter : letters) {
            shuffled += letter;
        }
        
        return shuffled;
    }
}
