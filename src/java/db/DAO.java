package db;


import app.*;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import org.hibernate.Hibernate;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.cfg.Configuration;
import org.hibernate.SessionFactory;
import stringgenerator.RandomString;
import utils.SubjectsByNameSort;


public class DAO {
    
    static SessionFactory sessionFactory = null;    
    static {
        sessionFactory = new Configuration().configure("/hibernate/hibernate.cfg.xml").buildSessionFactory();
    }
    public static void init() {        
    }
    
    private Session session = null;
    
    public DAO() {    
        init();
        session = sessionFactory.openSession();       
    }
    
    public void close() {
        session.close();
    }
    
    public ArrayList<Question> getAllApprovedQuestions() {
        
        session.beginTransaction();        
        Query query = session.createQuery("from Question"); 
        List allQuestions = query.list();        
        session.getTransaction().commit();
        
        ArrayList<Question> approvedQuestions = new ArrayList<>();
        for (Object object : allQuestions) {
            Question question = (Question)object;
            if (question.getState() == EntityState.approved) {                
                approvedQuestions.add(question);
            }
        }
        
        return approvedQuestions;
    }
    
    public ArrayList<Question> getAllQuestionsWithNewComments() {
        
        session.beginTransaction();        
        Query query = session.createQuery("from Question"); 
        List allQuestions = query.list();        
        session.getTransaction().commit();
        
        ArrayList<Question> questionsWithNewComments = new ArrayList<>();
        for (Object object : allQuestions) {
            Question question = (Question)object;
            Set<Comment> comments = question.getComments();
            if (comments != null) {
                for (Comment comment : question.getComments()) {
                    if (comment.getState() == EntityState.undefined) {                
                        questionsWithNewComments.add(question);
                        break;
                    }
                }      
            }
        }
        
        return questionsWithNewComments;
    }
    
    public ArrayList<Question> getAllUndefinedQuestions() {
        
        session.beginTransaction();        
        Query query = session.createQuery("from Question"); 
        List allQuestions = query.list();        
        session.getTransaction().commit();
        
        ArrayList<Question> undefinedQuestions = new ArrayList<>();
        for (Object object : allQuestions) {
            Question question = (Question)object;
            if (question.getState() == EntityState.undefined) {                
                undefinedQuestions.add(question);
            }
        }
        
        return undefinedQuestions;
    }
    
    public ArrayList<Question> getUserQuestions(ActiveUser activeUser) {
        
        session.beginTransaction();        
        Query query = session.createQuery("from Question where activeUser = " + activeUser.getId()); 
        List userQuestionsList = query.list();        
        session.getTransaction().commit();
        
        ArrayList<Question> userQuestions = new ArrayList<>();
        for (Object object : userQuestionsList) {
            Question question = (Question)object;
            Hibernate.initialize(question);
            Hibernate.initialize(question.getSubjects());
            userQuestions.add(question);
        }
        
        return userQuestions;
    }
    
    public ArrayList<Subject> getUserSubjects(ActiveUser activeUser) {
        
        session.beginTransaction();        
        Query query = session.createQuery("from Subject where activeUser = " + activeUser.getId()); 
        List userSubjectsList = query.list();        
        session.getTransaction().commit();
        
        ArrayList<Subject> userSubjects = new ArrayList<>();
        for (Object object : userSubjectsList) {
            userSubjects.add((Subject)object);
        }
        
        return userSubjects;
    }    
    
    public int getAllUndefinedQuestionsSize() {
        
        session.beginTransaction();        
        Query query = session.createQuery("from Question"); 
        List allQuestions = query.list();        
        session.getTransaction().commit();
        
        int count = 0;
        for (Object object : allQuestions) {
            Question question = (Question)object;
            if (question.getState() == EntityState.undefined) {                
                count++;
            }
        }
        
        return count;        
    }
    
    public Object getNewCommentsSize() {
        
        session.beginTransaction();        
        Query query = session.createQuery("from Comment"); 
        List comments = query.list();        
        session.getTransaction().commit();
        
        int count = 0;
        for (Object object : comments) {
            Comment comment = (Comment)object;
            if (comment.getState() == EntityState.undefined) {                
                count++;
            }
        }
        
        return count; 
    }
    
    public ArrayList<Question> getAllDiscardQuestions() {
        
        session.beginTransaction();        
        Query query = session.createQuery("from Question"); 
        List allQuestions = query.list();        
        session.getTransaction().commit();        
        
        ArrayList<Question> discardQuestions = new ArrayList<>();
        for (Object object : allQuestions) {
            Question question = (Question)object;
            if (question.getState() == EntityState.discard) {  
                //Hibernate.initialize(question);
                discardQuestions.add(question);
            }
        }
        
        return discardQuestions;
    }
    
    public Question getQuestionByNumberString(String questionNumberString) {  
        
        session.beginTransaction();
        
        Question question = null;
        try {
            question = (Question)session.load(Question.class, Long.parseLong(questionNumberString)); 
            Hibernate.initialize(question.getActiveUser());
            Hibernate.initialize(question.getComments());
            
            for (Comment comment : question.getComments()) {
                Hibernate.initialize(comment.getActiveUser());
            }
            
            Hibernate.initialize(question.getSubjects());
            
            /*
            HashSet<Subject> approveSubjects = new HashSet<>();
            for (Subject subject : question.getSubject()) {
                if (subject.getState() == EntityState.approved) {
                    approveSubjects.add(subject);
                }
            }*/
            
        } catch (Exception ex) {
            question = null;
            session.getTransaction().commit();
            throw(ex);
        }
        
        session.getTransaction().commit();        
        return question;
    }

    public boolean addQuestion(Question question) {
        
        session.beginTransaction();
        
        try {
            
            session.saveOrUpdate(question);  
            session.getTransaction().commit();
            return true;
        } catch (Exception ex) {
            ex.printStackTrace();
            session.getTransaction().rollback();
            return false;
        }
    }
    
    public boolean addComment(Comment comment) {
        
        session.beginTransaction();
        
        try {
            
            session.saveOrUpdate(comment); 
            session.getTransaction().commit();
            return true;
        } catch (Exception ex) {
            ex.printStackTrace();
            session.getTransaction().rollback();
            return false;
        }
    }
    
    
    public boolean addTemporaryUser(TemporaryUser temporaryUser) {
        
        session.beginTransaction();
        
        try {
            
            session.saveOrUpdate(temporaryUser);
            session.getTransaction().commit();
            
        } catch (Exception ex) {
            ex.printStackTrace();
            session.getTransaction().rollback();
            return false;
        }
        
        return true;
    }
    
    public TemporaryUser getTemporaryUserByConfirmationString(String confirmationString) {  
        
        session.beginTransaction();        
        
        TemporaryUser result = null;

        Query query = session.createQuery("from TemporaryUser where confirmationString = '" + confirmationString + "'");
        List queryList = query.list();

        if (!queryList.isEmpty()) {
            result = (TemporaryUser)queryList.get(0);
            
            Date nowDate = new Date();
            
            if (result.getDateValid() <= nowDate.getTime()) {
                result = null;
            }
        }

        session.getTransaction().commit();
        return result;            
    }    
    
    public void setRoleAction(ActiveUser user, String role) {        

        Query query = session.createQuery("from UserRole where userName = '" + user.getLogin() + "'"); 
        List roles = query.list();  

        for (Object object : roles) {
            session.delete(object);
        }

        session.saveOrUpdate(new UserRole(user, role));            
    }
    
    void addUserAction(ActiveUser user){
        
        session.saveOrUpdate(user);
    }
    
    
    public boolean addUser(ActiveUser user) {
        
        session.beginTransaction();
                        
        try {
            
            addUserAction(user);
            setRoleAction(user, "user");
            
            session.getTransaction().commit();   

        } catch (Exception ex) {            
            session.getTransaction().rollback();
            return false;
        }
        
        return true;
    }
    
    public boolean setRole(ActiveUser user, String role) {
        
        session.beginTransaction();
                        
        try {
            
            setRoleAction(user, role);
            
            session.getTransaction().commit();   

        } catch (Exception ex) {            
            session.getTransaction().rollback();
            return false;
        }
        
        return true;
    }
    
    public boolean ifTemporaryUserExistByName(String name) {
        
        session.beginTransaction();        
        
        Query query = session.createQuery("from TemporaryUser where login = '" + name + "'");
        List queryList = query.list();
        session.getTransaction().commit();
        
        for (Object object : queryList) {
            TemporaryUser temporaryUser = (TemporaryUser)object;
            if (temporaryUser.getDateValid().compareTo(new Date().getTime()) > 0) {
                return true;
            }
        }

        return false;   
    }
    
    public boolean ifActiveUserExistByName(String name) {
        
        session.beginTransaction();        
        
        Query query = session.createQuery("from ActiveUser where login = '" + name + "'");
        List queryList = query.list();
        session.getTransaction().commit();
        
        if (!queryList.isEmpty()) {
            return true;
        }

        return false;   
    }
    
    public ActiveUser getActiveUserByName(String name) {
        
        session.beginTransaction();        
        
        Query query = session.createQuery("from ActiveUser where login = '" + name + "'");
        List queryList = query.list();
        session.getTransaction().commit();
        
        if (!queryList.isEmpty()) {
            return (ActiveUser)queryList.get(0);
        }

        return null;   
    }
    
    public boolean checkActiveUserIfDisable(String name) {
        
        ActiveUser activeUser = getActiveUserByName(name);
        
        if (activeUser != null) {
            if (activeUser.getIfDisable() != null) {
                return activeUser.getIfDisable(); 
            }
        } 
        
        return false;           
    }
    
    public ActiveUser getActiveUserByNumberString(String id) {
        
        session.beginTransaction();        
        
        Query query = session.createQuery("from ActiveUser where id = '" + id + "'");
        List queryList = query.list();
        session.getTransaction().commit();
        
        if (!queryList.isEmpty()) {
            ActiveUser activeUser = (ActiveUser)queryList.get(0);
            return activeUser;
        }

        return null;   
    }
    
    public boolean changePassword(ActiveUser activeUser, String newPassword) {
        
        session.beginTransaction();        
        
        try { 
            activeUser.setPassword(newPassword);
            session.saveOrUpdate(activeUser);

            session.getTransaction().commit();
            return true;
            
        } catch (Exception ex) {
            session.getTransaction().rollback();
            return false;
        }
    }

    public Subject getSubjectByName(String name) {
        
        session.beginTransaction();        
        
        Query query = session.createQuery("from Subject where name = '" + name + "'");
        List queryList = query.list();
        session.getTransaction().commit();
        
        if (!queryList.isEmpty()) {
            return (Subject)queryList.get(0);
        }

        return null;   
    }
    
    public ArrayList<Subject> getAllApprovedSubjects() {
        
        session.beginTransaction();        
        Query query = session.createQuery("from Subject"); 
        List allSubjects = query.list();        
        session.getTransaction().commit();
        
        ArrayList<Subject> result = new ArrayList<>();
        for (Object object : allSubjects) {
            Subject subject = (Subject)object;
            if (subject.getState() == EntityState.approved) {
                //result.add(new Subject(subject.getId(), subject.getName()));
                Hibernate.initialize(subject.getQuestions());
                result.add(subject);
            }
        }
        
        return result;
    }
    
    public ArrayList<Subject> getAllApprovedSubjectsForList() {
        
        ArrayList<Subject> allApprovedSubjects = getAllApprovedSubjects();
        ArrayList<Subject> subjects = new ArrayList<>();
        ArrayList<Subject> groups = new ArrayList<>();
        for (Subject subjectApproved : allApprovedSubjects) {
            if (!subjectApproved.getName().equals("All questions")) {
                if (subjectApproved.getIfGroup()) {
                    groups.add(subjectApproved);
                } else {
                    subjects.add(subjectApproved);
                }
            }
        }
        
        Collections.sort(subjects, new SubjectsByNameSort());
        Collections.sort(groups, new SubjectsByNameSort());
        
        ArrayList<Subject> result = new ArrayList<>();
        for (Subject subject : subjects) {
            result.add(subject);
        }
        for (Subject group : groups) {
            result.add(group);
        }
        
        return result;
    }
    
    public ArrayList<ActiveUser> getAllUsers() {
        
        session.beginTransaction();        
        Query query = session.createQuery("from ActiveUser"); 
        List allActiveUser = query.list();        
        session.getTransaction().commit();
        
        ArrayList<ActiveUser> result = new ArrayList<>();
        for (Object object : allActiveUser) {
            ActiveUser activeUser = (ActiveUser)object;
            result.add(activeUser);
        }
        
        return result;
    }
    
    public boolean changeUserRole(ActiveUser user) {
        
        session.beginTransaction();        
        
        Query query = session.createQuery("from UserRole where userName = '" + user.getLogin() + "'"); 
        List queryList = query.list();
        session.getTransaction().commit();
        
        String role = "user";
        for (Object object : queryList) {
            UserRole userRole = (UserRole)object;
            if (userRole.getRoleName().equals("admin")) {
                role = "admin";
            }
        } 
        
        if (role.equals("user")) {
            return setRole(user, "admin");
        } else {
            return setRole(user, "user");
        }
    }
    
    public void getUsersRoles(ArrayList<ActiveUser> users) {
                
        ArrayList<ActiveUser> usersToRemove = new ArrayList<>();
        
        for (ActiveUser user : users) {
            session.beginTransaction();        
            Query query = session.createQuery("from UserRole where login = '" + user.getLogin() + "'");            
            List queryList = query.list();
            session.getTransaction().commit();

            if (!queryList.isEmpty()) {
                if (queryList.size() == 1) {
                    UserRole userRole = (UserRole)queryList.get(0);
                    String userRoleName = userRole.getRoleName();
                    if (userRoleName.equals("admin") || userRoleName.equals("user")) {
                        user.setRole(userRole);   
                    }   else {
                        usersToRemove.add(user);
                    }
                } else {
                    usersToRemove.add(user);
                }               
            } else {
                usersToRemove.add(user);
            }            
        }
        
        for (ActiveUser user : usersToRemove) {
            users.remove(user);
        }
    }    
    
    public ArrayList<Subject> getAllDiscardSubjects() {
        
        session.beginTransaction();        
        Query query = session.createQuery("from Subject"); 
        List allSubjects = query.list();        
        session.getTransaction().commit();
        
        ArrayList<Subject> result = new ArrayList<>();
        for (Object object : allSubjects) {
            Subject subject = (Subject)object;
            if (subject.getState() == EntityState.discard) {
                //result.add(new Subject(subject.getId(), subject.getName()));
                result.add(subject);
            }
        }
        
        return result;
    }
    
    public ArrayList<Subject> getAllUndefinedSubjects() {
        
        session.beginTransaction();        
        Query query = session.createQuery("from Subject"); 
        List allSubjects = query.list();        
        session.getTransaction().commit();
        
        ArrayList<Subject> result = new ArrayList<>();
        for (Object object : allSubjects) {
            Subject subject = (Subject)object;
            if (subject.getState() == EntityState.undefined) {
                //result.add(new Subject(subject.getId(), subject.getName()));
                result.add(subject);
            }
        }
        
        return result;
    }
    
    public int getAllUndefinedSubjectsSize() {
        
        session.beginTransaction();        
        Query query = session.createQuery("from Subject"); 
        List allSubjects = query.list();        
        session.getTransaction().commit();
        
        int count = 0;
        for (Object object : allSubjects) {
            Subject subject = (Subject)object;
            if (subject.getState() == EntityState.undefined) {
                //result.add(new Subject(subject.getId(), subject.getName()));
                count++;
            }
        }
        
        return count;
    }    
    
    public Comment getCommentByNumberString(String numberString) {
        
        session.beginTransaction();        
        
        Query query = session.createQuery("from Comment where id = '" + numberString + "'");
        List queryList = query.list();
        session.getTransaction().commit();
        
        if (!queryList.isEmpty()) {
            return (Comment)queryList.get(0);
        }

        return null;   
    }

    public boolean setCommentText(String commentNumberString, String commentText) {
        
        Comment comment = getCommentByNumberString(commentNumberString);            
         
        if (comment != null) {
        
            try {           
                session.beginTransaction();             
                comment.setText(commentText);  
                session.saveOrUpdate(comment);            
                session.getTransaction().commit();    
                return true;
            } catch (Exception ex) {
                ex.printStackTrace();
                session.getTransaction().rollback();            
                return false;
            }
        } else {
            return false;            
        }
    }
    
     public boolean setCommentState(String commentNumberString, String commentState) {
         
        Comment comment = getCommentByNumberString(commentNumberString);            
         
        if (comment != null) {
        
            try {
                EntityState state = EntityState.stateByString(commentState);
                
                if (state != null) {
                    comment.setState(state);
                    session.beginTransaction();                     
                    session.saveOrUpdate(comment);            
                    session.getTransaction().commit();                    
                    return true;
                } else {
                    return false;
                }
            } catch (Exception ex) {
                ex.printStackTrace();
                session.getTransaction().rollback();            
                return false;
            }
        } else {
            return false;            
        }
    }

    public boolean addSubject(Subject subject) {
        
        session.beginTransaction();
        
        try {
            
            session.saveOrUpdate(subject);  
            session.getTransaction().commit();  
            return true;
            
        } catch (Exception ex) {
            ex.printStackTrace();
            session.getTransaction().rollback();            
            return false;
        }
    }

    public Subject getSubjectByNumberString(String subjectId) {
        
        session.beginTransaction();        
        
        Query query = session.createQuery("from Subject where id = '" + subjectId + "'");
        List queryList = query.list();
        session.getTransaction().commit();
        
        if (!queryList.isEmpty()) {
            Subject subject = (Subject)queryList.get(0);
            Hibernate.initialize(subject.getQuestions());
            return subject;
        }

        return null; 
    }
    
    public Subject getSubjectByNumberStringWithApprovedQuestion(String subjectId) {
        
        Subject subject = getSubjectByNumberString(subjectId);
        
        if (subject == null) {
            return null; 
        } else {
            Subject result = new Subject();
            result.setId(subject.getId());
            result.setName(subject.getName());
            result.setState(subject.getState());
            
            Set<Question> tmp = new HashSet<Question>();
            for (Question question : subject.getQuestions()) {
                if (question.getState() == EntityState.approved) {
                    tmp.add(question);
                }
            }
            result.setQuestions(tmp);
            
            return result;
        }
    }

    public boolean userDisable(ActiveUser activeUser) {
        
        session.beginTransaction();
        
        Boolean ifDisable = activeUser.getIfDisable();
        if (ifDisable == null) {
            activeUser.setIfDisable(true);
        } else {
            activeUser.setIfDisable(!ifDisable);
        }
        
        try {
            
            session.saveOrUpdate(activeUser);  
            session.getTransaction().commit();  
            return true;
            
        } catch (Exception ex) {
            ex.printStackTrace();
            session.getTransaction().rollback();            
            return false;
        }
    }  
    
    public Long getNextQuestionId(Question currentQuestion, Subject subject) {        
        
        session.beginTransaction();        
        
        Query query = session.createQuery("from Question where id < '" + currentQuestion.getId() + "'");
        List queryList = query.list();
        session.getTransaction().commit();
        
        if (!queryList.isEmpty()) {
            
            Long nextQuestionId = null;
            for (Object object : queryList) {
                
                Question question = (Question)object;
                
                if (question.getState() == EntityState.approved) {
                    
                    Boolean bool = false;
                    for (Subject subjectInList : question.getSubjects()) {
                        if (subjectInList.getId().equals(subject.getId())) {
                            bool = true;
                            break;
                        }
                    }
                    
                    if (bool) {
                        
                        if (nextQuestionId != null) {
                            if ((question.getId() < currentQuestion.getId()) && (question.getId() > nextQuestionId)) {
                                nextQuestionId = question.getId();
                            }    
                        } else {
                            nextQuestionId = question.getId();
                        }
                    }
                }
            }
            return nextQuestionId;
        }
        
        return null; 
    }    
    
    public Long getPreviousQuestionId(Question currentQuestion, Subject subject) {
                
        session.beginTransaction();        
        
        Query query = session.createQuery("from Question where id > '" + currentQuestion.getId() + "'");
        List queryList = query.list();
        session.getTransaction().commit();
        
        if (!queryList.isEmpty()) {
            
            Long previousQuestionId = null;
            for (Object object : queryList) {
                
                Question question = (Question)object;
                
                if (question.getState() == EntityState.approved) {
                    
                    Boolean bool = false;
                    for (Subject subjectInList : question.getSubjects()) {
                        if (subjectInList.getId().equals(subject.getId())) {
                            bool = true;
                            break;
                        }
                    }
                    
                    if (bool) {
                    
                        if (previousQuestionId != null) {
                            if ((question.getId() > currentQuestion.getId()) && (question.getId() < previousQuestionId)) {
                                previousQuestionId = question.getId();
                            }    
                        } else {
                            previousQuestionId = question.getId();
                        }             
                    }
                }
            }
            return previousQuestionId;
        }
        
        return null; 
    }    
    
    public ActiveUserSession getActiveUserSessionToken(String token) {
        
        session.beginTransaction();        
        
        Query query = session.createQuery("from ActiveUserSession where token = '" + token + "'");
        List queryList = query.list();
        session.getTransaction().commit();
        
        if (!queryList.isEmpty()) {
            return (ActiveUserSession)queryList.get(0);
        }

        return null; 
    }

    public String setActiveUserSessionToken(String username) {
        String result = null;
        ActiveUser activeUser = getActiveUserByName(username);
        
        if (activeUser != null) {
            RandomString randomString = new RandomString(40);
            String resultTemp = randomString.nextString();
            
            ActiveUserSession activeUserSession = new ActiveUserSession();
            activeUserSession.setActiveUser(activeUser);
            activeUserSession.setToken(resultTemp);
            activeUserSession.setDateValid(new Date().getTime() + (60*60*24*30*1000));
            
            session.beginTransaction();  
            try {
                session.saveOrUpdate(activeUserSession);  
                result = resultTemp;
            } catch (Exception ex) {
                ex.printStackTrace();
                session.getTransaction().rollback();            
            }
        }
        
        return result;
    }

    public ActiveUser getTokenUser(String userToken) {
        
        ActiveUserSession activeUserSession = getActiveUserSessionToken(userToken);
        
        if (activeUserSession != null) {
            ActiveUser activeUser = activeUserSession.getActiveUser();
            
            boolean ifDisable = checkActiveUserIfDisable(activeUser.getName());
            
            if (!ifDisable) {
                return activeUser;
            }
        }
        
        return null;
    } 
}