package utils;


import app.Comment;
import java.util.Comparator;


public class CommentByDateSort implements Comparator<Comment> {
    
    @Override
    public int compare(Comment one, Comment two) { 
        
        if (one.getDate() != null) {
            if (two.getDate() != null) {
                return one.getDate().compareTo(two.getDate());
            } else {             
                return 1;
            }
        } else {
            return -1;
        }
    }
}
