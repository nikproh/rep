package utils;


import javax.servlet.http.HttpServletRequest;


public class Popup {
    Boolean popupWindow = false;
    Boolean popupGood = true;
    String popupText = "";
    
    public Popup(Boolean popupWindowIn, Boolean popupGoodIn, String popupTextIn) {
        popupWindow = popupWindowIn; 
        popupGood = popupGoodIn;
        popupText = popupTextIn;        
    }
    
    public void setInRequest(HttpServletRequest request) {
        request.setAttribute("popupWindow", popupWindow);
        request.setAttribute("popupGood", popupGood);
        request.setAttribute("popupText", popupText);
    }
}
