package utils;


import app.Question;
import java.util.Comparator;


public class QuestionsById implements Comparator<Question> {
    
    @Override
    public int compare(Question one, Question two) {        
        
        if (one.getDateCreation() != null) {
            if (two.getDateCreation() != null) {
                return one.getId().compareTo(two.getId());
            } else {             
                return 1;
            }
        } else {
            return -1;
        }
    }
}