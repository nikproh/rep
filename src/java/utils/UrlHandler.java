package utils;


import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javax.servlet.http.HttpServletRequest;


public class UrlHandler {
    
    public static String getUrl(HttpServletRequest request, String path) {
        return request.getContextPath() + path;
    }
    
    /*
    public static String getCurrentPageUrl(HttpServletRequest request) {
        
        String result = null;
        
        String referrerPage = request.getHeader("referer");
        
        if (referrerPage != null) {
            int port = request.getServerPort();
            
            String uriStarter = request.getScheme() + "://" +  request.getServerName() + ":" + port;      

            Pattern pattern = Pattern.compile(uriStarter + "(.*)");                   
            Matcher m = pattern.matcher(referrerPage);

            if (m.matches()) {
                result = m.group(1);
            }
        }
        
        return result;
    }*/  
    
    public static String getCurrentPagePath(HttpServletRequest request) {
        
        String result = null;
        
        String referrerPage = request.getHeader("referer");
        
        if (referrerPage != null) {
            int port = request.getServerPort();
            
            String uriStarter = request.getScheme() + "://" +  request.getServerName() + ":" + port + request.getContextPath();            

            Pattern pattern = Pattern.compile(uriStarter + "(.*)");                   
            Matcher m = pattern.matcher(referrerPage);

            if (m.matches()) {
                result = m.group(1);
            }
        }
        
        return result;
    }
}
