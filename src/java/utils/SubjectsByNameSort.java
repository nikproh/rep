package utils;


import app.Subject;
import java.util.Comparator;


public class SubjectsByNameSort implements Comparator<Subject> {
    
    @Override
    public int compare(Subject one, Subject two) {        
        
        if (one.getName() != null) {
            if (two.getName() != null) {
                return one.getName().compareTo(two.getName());
            } else {             
                return 1;
            }
        } else {
            return -1;
        }
    }
}