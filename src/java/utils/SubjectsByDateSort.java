package utils;


import app.Subject;
import java.util.Comparator;


public class SubjectsByDateSort implements Comparator<Subject> {
    
    @Override
    public int compare(Subject one, Subject two) {        
        
        if (one.getDateCreation() != null) {
            if (two.getDateCreation() != null) {
                return one.getDateCreation().compareTo(two.getDateCreation());
            } else {             
                return 1;
            }
        } else {
            return -1;
        }
    }
}