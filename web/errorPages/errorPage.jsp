<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="t" tagdir="/WEB-INF/tags" %>


<t:headerBodyFooter>
    
    <jsp:attribute name="title">    
        Error
    </jsp:attribute>
    
    <jsp:attribute name="header">    
        <jsp:include page="/header" />
    </jsp:attribute>
    
    <jsp:attribute name="content">
        <%@include file="errorPageDiv.jsp" %> 
    </jsp:attribute>
    
    <jsp:attribute name="footer">
        <%@include file="../pages/footer.jsp" %> 
    </jsp:attribute>
        
    <jsp:body>        
    </jsp:body>
        
</t:headerBodyFooter>