<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>


<div class="errorPageDiv">
    
    <div class="pageTitleDiv">
        Authorization error
    </div>

    <div class="errorPageTextDiv">
        To do that you must be login.<br>
        To login go <a href="${pageContext.request.contextPath}/loginForm"><u>here</u></a>.<br>
        To registration go <a href="${pageContext.request.contextPath}/registrationForm"><u>here</u></a>.    
    </div>
</div>