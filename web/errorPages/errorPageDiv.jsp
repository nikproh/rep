<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>


<div class="errorPageDiv">
    
    <div class="pageTitleDiv">
        Error
    </div>

    <div class="errorPageTextDiv">
        Some unknown error has occurred :(
    </div>
</div>