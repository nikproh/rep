<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
        

<div class="errorPageDiv">
    
    <div class="pageTitleDiv">
        Authorization error
    </div>

    <div class="errorPageTextDiv">
        To do that you must be logout.<br>
        To logout go <a href="${pageContext.request.contextPath}/logout"><u>here</u></a>.        
    </div>
</div>
