<%@tag description="Header Body Footer" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@attribute name="title" fragment="true" %>
<%@attribute name="header" fragment="true" %>
<%@attribute name="content" fragment="true" %>
<%@attribute name="footer" fragment="true" %>


<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title><jsp:invoke fragment="title"/></title>

        <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/css/css.css">   
        <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/prettify/prettify.css">
        
        <link rel="shortcut icon" href="${pageContext.request.contextPath}/images/favicon.ico" type="image/x-icon">
        
        <script type="text/javascript" src="${pageContext.request.contextPath}/js/jquery-2.2.0.min.js"></script> 
        <script type="text/javascript" src="${pageContext.request.contextPath}/prettify/prettify.js"></script>
        <script type="text/javascript" src="${pageContext.request.contextPath}/md5/md5.js"></script>
    </head>
    <body onload="prettyPrint()">      
            
        <c:choose>
            <c:when test="${setPageUrl ne null}">              
                <script>
                    window.history.pushState("", "", '${setPageUrl}');
                </script>
            </c:when>
        </c:choose>
        
        <div class="wrapper">

            <div class="header">
                <jsp:invoke fragment="header"/>
            </div>

            <div class="content">
                <jsp:invoke fragment="content"/>
            </div>

            <div class="footer">
                <jsp:invoke fragment="footer"/>
            </div>

        </div>            
         
        <jsp:include page="/pages/popup.jsp"/>
    </body>
</html>