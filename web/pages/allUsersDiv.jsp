<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
    

<script>  
    
    jQuery(document).ready(function(){

        
        $(document).on('click', '.userRoleAdminButton, .userRoleButton', function(){
            
            var str = "userRole";
            var userNumber = this.id.substring(str.length);

            $("#userRoleChange" + userNumber).val("true");
            $("#userChangeForm" + userNumber).submit();
        });
        
        $(document).on('click', '.passwordResetButtonDiv', function(){

            var str = "passwordResetButtonDiv";
            var userNumber = this.id.substring(str.length);
 
            $("#passwordReset" + userNumber).val("true");
            $("#userChangeForm" + userNumber).submit();
        });
        
        $(document).on('click', '.userDisableButtonDiv', function(){

            var str = "userDisableButtonDiv";
            var userNumber = this.id.substring(str.length);
 
            $("#userDisable" + userNumber).val("true");
            $("#userChangeForm" + userNumber).submit();
        });
    });
</script> 

<div class="allUsersDiv">
    
    <div class="pageTitleDiv">
        Users list:
    </div>
    
    <div class="usersDiv">
        
        <c:choose>
            <c:when test="${allUsers eq null}">       
                <br>There are no users :(
            </c:when>

            <c:otherwise>                

                <c:choose>
                    <c:when test="${allUsersSize eq '0'}">       
                        <br>There are no users :(
                    </c:when>

                    <c:otherwise> 

                        <table class="usersTable">
                    
                            <c:forEach var="user" items="${allUsers}"> 

                                <tr>

                                    <td class="usersTableLeftColumn">
                                        <c:choose>
                                            <c:when test="${user.name eq null}">       
                                                ${user.login}
                                            </c:when>

                                            <c:otherwise>  
                                                ${user.login}(${user.name})
                                            </c:otherwise>
                                        </c:choose>                                        
                                    </td>

                                    <td class="usersTableRightColumn">
                                        <form method="POST" id="userChangeForm${user.id}" action="${pageContext.request.contextPath}/userChange">

                                            <input type="hidden" name="userId" value="${user.id}" />                        
                                            
                                            <div class="rolesSelectDiv">
                                                
                                                <input type="hidden" name="roles" id="userRoleChange${user.id}" value="false" />                                                        
                                                <c:choose>
                                                    <c:when test="${user.role.roleName eq 'admin'}">                                                               
                                                        <div class="userRoleAdminButton" id="userRole${user.id}">
                                                    </c:when>

                                                    <c:otherwise>
                                                        <div class="userRoleButton" id="userRole${user.id}">
                                                    </c:otherwise>
                                                </c:choose>
                                                            
                                                    ${user.role.roleName}
                                                </div>
                                                
                                            </div>
                                                
                                            <div class="passwordResetButtonDiv" id="passwordResetButtonDiv${user.id}">                                                    
                                                <input type="hidden" name="passwordReset" id="passwordReset${user.id}" value="false" />
                                                reset
                                            </div>
                                            
                                            <div class="userDisableButtonDiv" id="userDisableButtonDiv${user.id}"> 
                                                <c:choose>
                                                    <c:when test="${user.ifDisable eq true}">       
                                                        enable
                                                    </c:when>
                                                    <c:otherwise> 
                                                        disable
                                                    </c:otherwise>
                                                </c:choose>
                                                <input type="hidden" name="userDisable" id="userDisable${user.id}" value="false" />
                                            </div>
                                        </form>
                                    </td>

                                </tr>
                                
                            </c:forEach>
                        </table>    

                    </c:otherwise>
                </c:choose>

            </c:otherwise>
        </c:choose>
                    
    </div>
</div>