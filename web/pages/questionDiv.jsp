<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>


<script> 
    
    jQuery(document).ready(function(){
            
        $(document).on('click', '.questionResultChangeButtonDiv', function(){
            $("#questionInputDiv").show();
            $("#questionResultChangeButtonDiv").hide();
            $("#checkButtonDiv").hide();   
            $(".commentChangeButton").hide();
        });
        
        $(document).on('click', '.questionInputDiscardButtonDiv', function(){
            $("#questionInputDiv").hide();
            $("#questionResultChangeButtonDiv").show();            
            $("#answerResult").hide();
            $("#checkButtonDiv").show();
            $(".commentChangeButton").show();
            setQuestion();
        });
        
        $(document).on('click', '.questionInputSaveButtonDiv', function(){
            
            var checkQuestions = ifQuestionNumberIsOk();
            if (checkQuestions === 'good') {
                var subjects = "";
                var str = "subjectOption";

                $(".subjectDivSelect").each(function (i) {
                    var subjectId = this.id.substring(str.length);
                    subjects += subjectId + ",";
                });        
                
                if ((subjects === "") && ($("#questionInputNewSubject").val() === "")) {
                    $(".popupImage img").attr("src","${pageContext.request.contextPath}/images/fail.png");
                    $('#popupText').html('Error - there are too few subjects');
                    $('.popupShadow').show();
                } else {
                    $("#subjects").val(subjects);
                    $("#questionInputForm").submit();            
                }
            } else {
                $(".popupImage img").attr("src","${pageContext.request.contextPath}/images/fail.png");
                $('#popupText').html('Error - ' + checkQuestions);
                $('.popupShadow').show();
            }
        });        
                
        $(document).on('click', '.subjectDiv, .subjectDivSelect', function(){
            
            var className = $(this).attr('class');
            
            if (className === 'subjectDiv') {
                $(this).removeClass('subjectDiv').addClass('subjectDivSelect');
            } else {
                $(this).removeClass('subjectDivSelect').addClass('subjectDiv');
            }           
        });
        
        $(document).on('click', '.stateChangeButtonDiv', function() {
            $("#state").val($(this).attr("id"));
            
            $(".stateChangeButtonSelectDiv").removeClass('stateChangeButtonSelectDiv').addClass('stateChangeButtonDiv');
            $(this).removeClass('stateChangeButtonDiv').addClass('stateChangeButtonSelectDiv');
        });
        
        $(document).on('click', '.checkboxAnswer', function() {
            var str = "checkboxAnswer";
            var answerNumber = this.id.substring(str.length);           
            
            if ($("#checkboxAnswer" + answerNumber).attr("src") === "${pageContext.request.contextPath}/images/checkboxNoCheck.png") {
                $("#checkboxAnswer" + answerNumber).attr("src", "${pageContext.request.contextPath}/images/checkboxCheck.png");            
            } else {
                $("#checkboxAnswer" + answerNumber).attr("src", "${pageContext.request.contextPath}/images/checkboxNoCheck.png");            
            }
        });
        
        $(document).on('click', '.radioAnswer', function() {
            var str = "radioAnswer";
            var answerNumber = this.id.substring(str.length);           
            
            $(".radioAnswer").attr("src", "${pageContext.request.contextPath}/images/radioNoCheck.png");
            $("#radioAnswer" + answerNumber).attr("src", "${pageContext.request.contextPath}/images/radioCheck.png");
        });        
        
        $(document).on('click', '.checkButtonDiv', function() {
            $(this).hide();
            
            var result = '';
            var x = 0;
            if (ifAnswerRight()) {
                result = "<table><tr><td class='answerRightWrongSign'><img src='${pageContext.request.contextPath}/images/rigth.png'/></td><td>You are right!</td></tr></table>";            
                x += 35;
            } else {                
                var rightAnswers = getRightAnswers();
                
                var rightAnswersText = '';
                for (var i = 0; i < rightAnswers.length; i++) {
                    rightAnswersText += getLetterByNumber(parseInt(rightAnswers[i], 10)) + ', ';
                }            
                if (rightAnswersText.length > 0) {
                    rightAnswersText = rightAnswersText.substr(0, rightAnswersText.length - 2);
                }
                
                result = "<table><tr><td class='answerRightWrongSign'><img src='${pageContext.request.contextPath}/images/wrong.png'/></td><td>Wrong! This is correct answers: " + rightAnswersText + "</td></tr></table>";                
            }
            $("#answerResult").offset(function(i, val){
                return {top:val.top, left:x};
            });    
            $("#answerResult").html(result);            
            $("#answerResult").show();
        });  
        
        <c:choose>
            <c:when test="${ifAuthtorize eq 'true'}">       
                var ifAuth = true;
            </c:when>

            <c:otherwise> 
                var ifAuth = false;
            </c:otherwise>  
                                    
        </c:choose> 
        
        $(document).on('click', '.showCommentsDiv', function() {    
           
            var hideOrShow = $(this).html().trim().substring(0, 10);
            
            var bool = (collectionSize(inComments) > 0) || ifAuth;
            
            if (hideOrShow === '<a><u>Show') {
                $(this).html('<a><u>Hide comments</u></a>');
                if (bool) {
                    $(".questionCommentDiv").show();
                }
            } else {
                $(this).html('<a><u>Show comments (${commentsCount})</u></a>');  
                if (bool) {
                    $(".questionCommentDiv").hide();
                }
            }
        });
        
        $(document).on('click', '.commentChangeButton', function(){
            var str = "commentChangeButton";
            var commentNumber = this.id.substring(str.length);
                        
            $("#commentParagraph" + commentNumber + " .commentText").hide();         
            $("#commentChangeFormDiv" + commentNumber).show();
            
            $("#commentSubmitButton" + commentNumber).show();
            $("#commentDiscardButton" + commentNumber).show();            
            $(".commentChangeButton").hide();        
            $(".questionResultChangeButtonDiv").hide();  
            $(".commentButton1").hide();
            $(".commentButton2").hide();
        });
        
        $(document).on('click', '.commentDiscardButton', function(){
            setComments();    
            var str = "commentDiscardButton";
            var commentNumber = this.id.substring(str.length);

            $("#commentParagraph" + commentNumber + " .commentText").show();         
            $("#commentChangeFormDiv" + commentNumber).hide();
            
            $("#commentSubmitButton" + commentNumber).hide();
            $("#commentDiscardButton" + commentNumber).hide();            
            $(".commentChangeButton").show();        
            $(".questionResultChangeButtonDiv").show();
            $(".commentButton1").show();
            $(".commentButton2").show();
        });
        
        $(document).on('click', '.commentSubmitButton', function(){
            var str = "commentSubmitButton";
            var commentNumber = this.id.substring(str.length);
            
            $("#commentChangeForm" + commentNumber).submit();
        });
        
        $(document).on('click', '.commentButton1, .commentButton2', function(){
            
            var str = "commentAppButton";
            var commentNumber = this.id.substring(str.length);
            
            var newState = this.id.substring(7, 10);            
            if (newState === 'App') {
                $("#changeCommentStateForm" + commentNumber + " #state").val('approved');
            } else {
                $("#changeCommentStateForm" + commentNumber + " #state").val('discard');
            }
            
            $("#changeCommentStateForm" + commentNumber).submit();
        });
        
        $(document).on('click', '.commentSubmit', function(){
            $("#addCommentForm").submit();
        });
        
        <c:if test="${ifNew eq 'true'}"> 
            setNewQuestionExample();
        </c:if>         
        setQuestion();
        setComments();
        setSubjectWidth();
    });    
    
    function setNewQuestionExample() {
        var exampleText = "This is the example of question decoration. Below you can see the result.\nYou have to present at least one rigth answer and at least two answers over all\n";
        exampleText += "++++\nThis is the decoration of rigth answer\n----\nThis is the decoration of wrong answer\n----\nThis is the decoration of the code\n    code line\n    another code line\n++++\nLast answer";
        $("#questionTextHidden").html(exampleText);        
    }
    
    function ifQuestionNumberIsOk() {
        var rawText = $("#questionInputTextDiv textarea").val();
        var questionParts = getQuestionParts(rawText);
        var answers = questionParts[1];
        
        if (answers.length >= 2) {
                        
            for (var i = 0; i < answers.length; i++) { 
                if (answers[i][0] === true) {
                    return 'good';
                }
            }
            
            return 'there is no rigth answer'; 
            
        } else {        
            return 'there are too few answers';        
        }
    }
    
    function ifQuestionSubjectIsOk() {

        var varId = $(".subjectDivSelect").attr('id');
        if (varId !== undefined) {
            return 'good';
        }

        if ($("#questionInputNewSubject").val() !== '') {
            return 'good';
        }
        
        return 'there is no subject';
    }
    
    function getRightAnswers() {
    
        var rawText = $("#questionInputTextDiv textarea").val();
        var questionParts = getQuestionParts(rawText);
        var answers = questionParts[1];
        
        var rightAnswers = "";
        for (var i = 0; i < answers.length; i++) { 
            if (answers[i][0] === true) {
                rightAnswers += i;
            }
        }
        
        return rightAnswers;
    }
    
    function ifAnswerRight() {
        var rawText = $("#questionInputTextDiv textarea").val();
        var questionParts = getQuestionParts(rawText);
        var answers = questionParts[1];

        var correctAnswers = '';
        for (var i = 0; i < answers.length; i++) { 
            if (answers[i][0] === true) {
                correctAnswers += i;
            }
        }
        
        var userAnswers = '';
        $(".questionTableLeft img").each(function (i) {
            
            var src = $(this).attr("src");
            if ((src === "${pageContext.request.contextPath}/images/checkboxCheck.png") || (src === "${pageContext.request.contextPath}/images/radioCheck.png")) {
                userAnswers += i;
            }
        }); 
        
        return (correctAnswers === userAnswers);
    }
    
    var inSubjects = {};
    <c:forEach var="subject" items="${question.subjects}">        
        inSubjects["${subject.id}"] = "${subject.id}";
    </c:forEach> 
    
    function setQuestion() {
        $("#questionText").html($("#questionTextHidden").html());
        
        $("#${question.state}").removeClass("stateChangeButtonDiv");
        $("#${question.state}").addClass("stateChangeButtonSelectDiv");        
                           
        for(var key in inSubjects) {                        
            $("#subjectOption" + inSubjects[key]).removeClass("subjectDiv");
            $("#subjectOption" + inSubjects[key]).addClass("subjectDivSelect");
        }
    }
    
    function setSubjectWidth() {
        
        var barWidth = getScrollBarWidth();
        
        $(".subjectDivSelect").each(function (i) {
            var width = $(this).width();
            $(this).width(width - barWidth);
        });
        
        $(".subjectDiv").each(function (i) {
            var width = $(this).width();
            $(this).width(width - barWidth);
        });
    }
    
    function getScrollBarWidth() {
        var inner = document.createElement('p');
        inner.style.width = "100%";
        inner.style.height = "200px";

        var outer = document.createElement('div');
        outer.style.position = "absolute";
        outer.style.top = "0px";
        outer.style.left = "0px";
        outer.style.visibility = "hidden";
        outer.style.width = "200px";
        outer.style.height = "150px";
        outer.style.overflow = "hidden";
        outer.appendChild (inner);

        document.body.appendChild (outer);
        var w1 = inner.offsetWidth;
        outer.style.overflow = 'scroll';
        var w2 = inner.offsetWidth;
        if (w1 == w2) w2 = outer.clientWidth;

        document.body.removeChild (outer);

        return (w1 - w2);
    };
    
    function devidedToParts(rawText) {
        var question = "";
        var answers = new Array();        
        
        var entityType = "question";
        var entityText = "";
        
        var rawTextArrayLine = rawText.split("\n");
        for (var countLine = 0; countLine < rawTextArrayLine.length; countLine++) { 
            var line = rawTextArrayLine[countLine];
            if ((line === '++++') || (line === '----')) {
                                
                switch (entityType) {
                case 'question':
                    question = entityText;                    
                    break;
                case 'trueAnswer':
                    answers.push([true, entityText]);
                    break;
                case 'falseAnswer':
                    answers.push([false, entityText]);
                    break;
                }
                
                entityText = "";
                if (line === '++++') {
                    entityType = "trueAnswer";
                } else {
                    entityType = "falseAnswer";
                }
            } else {
                entityText += line + "\n";
            }
        }
        
        switch (entityType) {
        case 'question':
            question = entityText;
            break;
        case 'trueAnswer':
            answers.push([true, entityText]);
            break;
        case 'falseAnswer':
            answers.push([false, entityText]);
            break;
        }
        
        var result = [question, answers];
        
        return result;
    }
    
    function processTheParts(rawText) {
        
        var rawTextArrayLine = rawText.split("\n");
        var result = '';
        var ifCodeCurrent = false;
        
        for (var countLine = 0; countLine < rawTextArrayLine.length; countLine++) { 
            var line = rawTextArrayLine[countLine];
            
            var strStart = line.substring(0,4);
            if (strStart === '    ') {
                var strEnd = line.substring(4);
                if (!ifCodeCurrent) {
                    ifCodeCurrent = true;
                    result += "<pre class='prettyprint'>" + strEnd + "\n";
                } else {
                    result += strEnd + "\n";
                }
            } else {
                if (ifCodeCurrent) {
                    if (strStart === '') {
                        result += "\n";
                    } else {
                        result += "</pre>" + line + "<br>";
                        ifCodeCurrent = false;
                    }
                } else {
                    result += line + "<br>";
                }                    
            }
        }
        
        if (ifCodeCurrent) {
            result += "</pre><br>";                        
        } 
        
        if (result.length > 0) { 
            result = result.substring(0, result.length - 4)
        }
    
        return result;
    }
    
    function getQuestionParts(rawText) {
        
        var result = devidedToParts(rawText);
        
        var question = result[0];
        var answers = result[1];
        
        question = processTheParts(question);
        
        for (var i = 0; i < answers.length; i++) { 
            answers[i][1] = processTheParts(answers[i][1]);
        }
        result = [question, answers];

        return result;
    }
    
    function getAnswersCount(answers) { 
        
        var count = 0;
        for (var i = 0; i < answers.length; i++) { 
            if (answers[i][0] === true) {
                count++;
            }
        }
        
        return count;
    }
        
    function getLetterByNumber(i) { 
        return String.fromCharCode(97 + i);
    }
    
    function screeningText(rawText) { 
        
        var result = "";
        
        for (var i = 0; i < rawText.length; i++) {
            switch (rawText[i]) {
            case '<':
               result += '&lt;';
               break;
            case '>':
               result += '&gt;';
               break;
            case '&':
               result += '&amp;';
               break;   
            default:
               result += rawText[i];
            }
        }
        
        return result;
    }
    
    function convertText(rawText) { 
                
        rawText = screeningText(rawText);
        var questionParts = getQuestionParts(rawText);
        
        var question = questionParts[0];
        var answers = questionParts[1];
        
        var answersCount = getAnswersCount(answers);
        
        var resultText = '<table class="questionTable"><tr><td class="questionTableQuestionMark" colspan="2">';
        resultText += '<img src="${pageContext.request.contextPath}/images/questionMark.png"/>';
        resultText += '</td><td class="questionTableQuestion">' + question + '(Choose ' + answersCount + ' answers)</td></tr>';
        
        for (var countAnswers = 0; countAnswers < answers.length; countAnswers++) { 
            resultText += '<tr><td class="questionTableLeft">';
            
            if (answersCount < 2) {
                resultText += '<img src="${pageContext.request.contextPath}/images/radioNoCheck.png"/ class="radioAnswer" id="radioAnswer' + countAnswers + '">';
            } else {
                resultText += '<img src="${pageContext.request.contextPath}/images/checkboxNoCheck.png"/ class="checkboxAnswer" id="checkboxAnswer' + countAnswers + '">';
            }            
            
            resultText += '</td><td class="questionTableCenter">' + getLetterByNumber(countAnswers) + ')</td>';
            resultText += '<td class="questionTableRight">' + answers[countAnswers][1] + '</td></tr>';
        }
        
        resultText += '</table>';
        return resultText;
    }
    
    function setQuestionResultTextDiv() {        
        
        var rawText = $("#questionInputTextDiv textarea").val();
        
        if (oldResultText !== rawText) { 
        
            oldResultText = rawText;
            
            var resultText = convertText(rawText);
            
            $("#questionResultTextDiv").html(resultText);            
            $("#questionResultTextDiv pre").removeClass("prettyprinted");
            prettyPrint();   
        }
    }
    
    var inComments = {};
    <c:forEach var="comment" items="${comments}">
        inComments["${comment.id}"] = "${comment.id}";    
    </c:forEach>
        
    function collectionSize(collect) {     
        
        var len = 0;        
        for(var key in collect) { 
            len++;
        }        
        return len;
    }
    
    function setComment(id) {       
        var text = $("#commentChangeFormDiv" + id + " #commentTextHidden").val();
        
        $("#commentChangeFormDiv" + id + " #commentText").html(text);
        $("#commentParagraph" + id + " #commentText").html(screeningText(text));
        
        $("#commentSubmitButton" + id).hide();
        $("#commentDiscardButton" + id).hide();
    }
    
    function setComments() {
        for(var key in inComments) { 
            setComment(key);
        }
    }

    var oldResultText = null;    
    window.setInterval(function(){
        setQuestionResultTextDiv();  
    }, 100);    
</script>

<div class="questionDiv">
    
    <div class="pageTitleDiv">
        <%@include file="questionHead.jsp" %>
    </div>    
    
    <div class="questionFormDiv">        
    
        <c:choose>
            <c:when test="${question eq null}">       
                <br>There is no such question :(
            </c:when>

            <c:otherwise>  

                <c:choose>
                    <c:when test="${ifNew ne 'true'}">       
                        <div id="questionInputDiv" class="questionInputDiv" hidden>
                    </c:when>

                    <c:otherwise>  
                        <div id="questionInputDiv" class="questionInputDiv"> 
                    </c:otherwise>  
                </c:choose>

                    <form method="POST" id="questionInputForm" action="${pageContext.request.contextPath}/save">
                        
                        <textarea id="questionTextHidden" hidden>${question.text}</textarea>   
                        <input type="hidden" name="subject" value="${subject.id}">                        
                        
                        <c:choose>
                            <c:when test="${ifNew eq 'true'}">       
                                <input type="hidden" value="new" name="questionNumber"  />
                            </c:when>

                            <c:otherwise>  
                                <input type="hidden" value="${question.id}" name="questionNumber" />
                            </c:otherwise>  
                        </c:choose> 
                        
                        <div id="questionInputTextDiv" class="questionInputTextDiv">
                            <textarea name="questionText" id="questionText" wrap='off'></textarea>
                        </div>    
                                
                        <div class="questionInputSubjectDiv">

                            <input type="hidden" id="subjects" name="subjects" value="">
                            
                            <c:forEach var="subject" items="${allSubjects}">                         
                                <div class="subjectDiv" id="subjectOption${subject.id}">
                                    <c:choose>
                                        <c:when test="${subject.ifGroup eq 'false'}">       
                                            ${subject.name}
                                        </c:when>

                                        <c:otherwise>  
                                            <b>${subject.name}</b>
                                        </c:otherwise>  
                                    </c:choose> 
                                </div>
                            </c:forEach>
                        </div>   
                                
                        <div class="questionInputNewSubjectTextDiv">
                            Add new subject:
                        </div>
                        <div class="questionInputNewSubjectDiv">
                            <input type="text" name="newSubject" id="questionInputNewSubject"/>
                        </div>                    
                               
                        <c:if test="${ifNew ne 'true'}">                        
                            <c:if test="${ifAdmin eq 'true'}">     
                                <div id="questionInputStateDiv" class="questionInputStateDiv">                                 

                                    <input type="hidden" id="state" name="state" value="">

                                    <div class="stateChangeButtonDiv" id="approved">
                                        approved
                                    </div>
                                    <div class="stateChangeButtonDiv" id="discard">
                                        discard
                                    </div>
                                    <div class="stateChangeButtonDiv" id="undefined">
                                        undefined
                                    </div>                        

                                </div> 
                            </c:if>   
                        </c:if> 
                                
                    </form>
                            
                    <div class="questionInputSaveButtonDiv" id="questionInputSaveButtonDiv">
                        Save
                    </div>
                    
                    <c:if test="${ifNew ne 'true'}">                        
                        <div class="questionInputDiscardButtonDiv" id="questionInputDiscardButtonDiv">
                            Discard
                        </div>
                    </c:if> 
                </div>

                <div class="questionResultDiv" id="questionResultDiv">
                    
                    <c:if test="${ifNew ne 'true'}"> 
                        <c:if test="${ifAdmin eq 'true'}">   
                            <div class="questionResultChangeButtonDivWrapper">
                                <div class="questionResultChangeButtonDiv" id="questionResultChangeButtonDiv">
                                    Change
                                </div>
                            </div>
                        </c:if>
                    </c:if>
                    
                    <div id="questionResultTextDiv" class="questionResultTextDiv">                        
                    </div>                    
                    
                    <c:if test="${ifNew ne 'true'}"> 
                        
                        <div class="checkResultDiv">
                            <div class="checkButtonDiv" id="checkButtonDiv">
                                Check
                            </div>
                            
                            <div class="showCommentsDiv" id="showCommentsDiv">   
                                <c:choose>
                                    <c:when test="${commentShow == true}">        
                                        <a><u>Hide comments</u></a>
                                    </c:when>

                                    <c:otherwise>         
                                        <a><u>Show comments (${commentsCount})</u></a>
                                    </c:otherwise>
                                </c:choose>
                                
                            </div>
                            
                            <div class="nextPreviousQuestionButtonDiv" id="nextPreviousQuestionButtonDiv">
                                <table class="subjectTable">
                                    <tr>
                                        <td class="subjectTableLeftArrow">
                                            <c:if test="${nextQuestionId ne null}"> 
                                                <a href = '${pageContext.request.contextPath}/question/${subject.id}/${nextQuestionId}'><img src="${pageContext.request.contextPath}/images/leftArrow.png"/></a>
                                            </c:if>                                            
                                        </td>
                                        <td class="subjectTableRightArrow">                                            
                                            <c:if test="${previousQuestionId ne null}">                                                                  
                                                <a href = '${pageContext.request.contextPath}/question/${subject.id}/${previousQuestionId}'><img src="${pageContext.request.contextPath}/images/rightArrow.png"/></a>                                    
                                            </c:if>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <c:if test="${nextQuestionId ne null}"> 
                                                <a href = '${pageContext.request.contextPath}/question/${subject.id}/${nextQuestionId}'><u>${nextQuestionId}</u></a>
                                            </c:if>
                                        </td>
                                        <td class="subjectTableRightText">
                                            <c:if test="${previousQuestionId ne null}">                                                                  
                                                <a href = '${pageContext.request.contextPath}/question/${subject.id}/${previousQuestionId}'><u>${previousQuestionId}</u></a>                                    
                                            </c:if>
                                        </td>
                                    </tr>
                                </table>
                            </div>
                            
                            <div class="answerResult" id="answerResult" hidden> 
                            </div>
                        </div>

                    </c:if>  
                     
                </div> 
                        
                <c:if test="${ifNew ne 'true'}">  
                    <c:choose>
                        <c:when test="${commentShow == true}">        
                            <div class="questionCommentDiv" id="questionCommentDiv">
                        </c:when>

                        <c:otherwise>         
                            <div class="questionCommentDiv" id="questionCommentDiv" hidden>
                        </c:otherwise>
                    </c:choose>
                                
                        <c:forEach var="comment" items="${comments}"> 
                            <c:choose>
                                <c:when test="${ifAdmin eq 'true'}">        
                                    <c:choose>
                                        <c:when test="${comment.state eq 'approved'}">        
                                            <div id="commentParagraph${comment.id}" class="commentParagraph">
                                        </c:when>

                                        <c:when test="${comment.state eq 'undefined'}">        
                                            <div id="commentParagraph${comment.id}" class="commentParagraphUndefined">
                                        </c:when>        

                                        <c:otherwise>         
                                            <div id="commentParagraph${comment.id}" class="commentParagraphDiscard">
                                        </c:otherwise>
                                    </c:choose> 
                                </c:when>
                                    
                                <c:otherwise>         
                                    <div id="commentParagraph${comment.id}" class="commentParagraph">
                                </c:otherwise>
                            </c:choose>
                                
                                <div class="commentInformation"><small><b>&nbsp;&nbsp;&nbsp;&nbsp;${comment.activeUser.login} (${comment.dateStr})</b></small></div>    
                                
                                <div id="commentText" class="commentText" name="commentText"></div>
                                
                                <c:if test="${ifAdmin eq 'true'}">  
                                    
                                    <c:choose>
                                        <c:when test="${comment.state eq 'approved'}">        
                                            <div class="commentButton2" id="commentDisButton${comment.id}">
                                                Discard
                                            </div>
                                        </c:when>

                                        <c:when test="${comment.state eq 'undefined'}">        
                                            <div class="commentButton1" id="commentAppButton${comment.id}">
                                                Approved
                                            </div>
                                            <div class="commentButton2" id="commentDisButton${comment.id}">
                                                Discard
                                            </div>
                                        </c:when>        

                                        <c:otherwise>         
                                            <div class="commentButton2" id="commentAppButton${comment.id}">
                                                Approved
                                            </div>
                                        </c:otherwise>
                                    </c:choose> 
                                    
                                    <form action="${pageContext.request.contextPath}/changeCommentState/${comment.id}" method="post" id="changeCommentStateForm${comment.id}">
                                        <input type="hidden" id="state" name="state" value="">                                         
                                        <input type="hidden" id="questionId" name="questionId" value="${question.id}">
                                    </form> 
                                    
                                    <div class="commentChangeButton" id="commentChangeButton${comment.id}">
                                        Change
                                    </div>   
                                    
                                    <div class="commentSubmitButton" id="commentSubmitButton${comment.id}" hidden>
                                        Submit
                                    </div> 
                                    
                                    <div class="commentDiscardButton" id="commentDiscardButton${comment.id}" hidden>
                                        Discard
                                    </div>     
                                </c:if>  
                                
                                <div id="commentChangeFormDiv${comment.id}" class="commentChangeFormDiv" hidden> 
                                    <form action="${pageContext.request.contextPath}/changeComment/${comment.id}" method="post" id="commentChangeForm${comment.id}">
                                        <textarea id="commentTextHidden" hidden>${comment.text}</textarea>
                                        <textarea id="commentText" name="commentText" class="commentChangeFormText"></textarea>                                            
                                        <input type="hidden" id="questionId" name="questionId" value="${question.id}">
                                    </form>                                   
                                </div>
                                
                            </div>
                        </c:forEach>   

                        <c:if test="${ifAuthtorize eq 'true'}">              
                            <div id="addCommentDiv" class="addCommentDiv">
                                <form action="${pageContext.request.contextPath}/addComment" method="post" name="addCommentForm" id="addCommentForm">
                                    <p>
                                        <small>Add comment</small>
                                        <br>           
                                        <textarea name="commentText" id="commentText" cols="80" rows="3"></textarea>
                                        <input type="hidden" id="questionId" name="questionId" value="${question.id}" />                    
                                    </p>
                                    
                                    <div class="commentSubmit" id="commentSubmit">
                                        Submit
                                    </div>
                                </form>
                            </div>
                        </c:if>   
                    </div>   
                </c:if>
                        
                </div>
            </c:otherwise>
        </c:choose>
            
</div>
 
  
