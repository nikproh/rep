<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
 

<script> 
    jQuery(document).ready(function(){
    
        $(document).on('click', '#enterToSiteButton', function(){
            $("#enterToSiteForm").submit();
        });
    });
</script>

<c:choose>
    <c:when test="${user eq null}">       
        <br>There are no profile :(
    </c:when>

    <c:otherwise> 
        <div class="profileDiv">
    
            <div class="pageTitleDiv">
                My profile: 
            </div>    

            <div class="profileFormDiv">
                <table>
                    <tr>
                        <td class="formText">
                            login
                        </td>
                        <td class="formInput">
                            <input type="text" name="username" id="username" value="${user.login}" disabled='disabled' />
                        </td>
                    </tr>                       
                    <tr>
                        <td class="formText">
                            name
                        </td>
                        <td class="formInput">
                            <input type="text" name="name" id="name" value="${user.name}" disabled='disabled'/>
                        </td>
                    </tr> 
                </table>   
            </div>   

            <div class="profileFormResetDiv">
                <a href="${pageContext.request.contextPath}/passwordChangeForm">Change password</a>
            </div>
        </div>
            
    </c:otherwise> 
</c:choose>