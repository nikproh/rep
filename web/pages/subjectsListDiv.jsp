<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
    

<script>  
    
    jQuery(document).ready(function(){
        
        $(document).on('click', '.stateChangeButton', function(){

            var str = "";

            if (this.id.substring(0, "discard".length) === "discard") {
                str = "discard";
            } else if (this.id.substring(0, "approved".length) === "approved") {
                str = "approved";
            } else {
                str = "undefined";
            }                

            var subjectNumber = this.id.substring(str.length); 
            $("#newState" + subjectNumber).val(str);
            $("#subjectStateChangeForm" + subjectNumber).submit();
        });
        
        $(document).on('click', '.renameButton', function(){
            var str = 'renameButton';
            var subjectNumber = this.id.substring(str.length); 
            $("#subjectRenameForm" + subjectNumber).submit();            
        }); 
        
        $(document).on('click', '.subjectToGroupButton, .subjectToGroupSelectButton', function(){
            var str = 'subjectToGroupButton';
            var subjectNumber = this.id.substring(str.length); 
            $("#subjectToGroup" + subjectNumber).submit();            
        });        
    });
</script> 

<c:choose>
    <c:when test="${subjects eq null}">       
        <br>There are no subjects :(
    </c:when>

    <c:otherwise>                
        
        <c:choose>
            <c:when test="${subjectsSize eq '0'}">       
                <br>There are no subjects :(
            </c:when>

            <c:otherwise> 
                <table class="subjectTableDiv">
                    
                    <c:forEach var="subject" items="${subjects}"> 
                        <tr>

                            <td class="subjectTableLeftColumn">
                                <a href = '${pageContext.request.contextPath}/subject/${subject.id}'><u>${subject.name}</u></a>
                            </td>

                            <td class="subjectTableRightColumn">
                                <form method="POST" id="subjectStateChangeForm${subject.id}" action="${pageContext.request.contextPath}/subjectStateChange">
                                    <input type="hidden" name="subjectId" value="${subject.id}" />
                                    <input type="hidden" name="oldState" value="${subjectType}" />
                                    <input type="hidden" name="newState" id="newState${subject.id}" value="" />
                                    <c:choose>
                                        <c:when test="${subjectType eq 'approved'}">  
                                            <div class="stateChangeButton" id="discard${subject.id}">
                                                discard
                                            </div>
                                            <div class="stateChangeButton" id="undefined${subject.id}">
                                                undefined
                                            </div> 
                                        </c:when>
                                        <c:when test="${subjectType eq 'discard'}">
                                            <div class="stateChangeButton" id="approved${subject.id}">
                                                approved
                                            </div>
                                            <div class="stateChangeButton" id="undefined${subject.id}">
                                                undefined
                                            </div> 
                                        </c:when>
                                        <c:when test="${subjectType eq 'undefined'}">
                                            <div class="stateChangeButton" id="approved${subject.id}">
                                                approved
                                            </div>
                                            <div class="stateChangeButton" id="discard${subject.id}">
                                                discard
                                            </div>                                             
                                        </c:when>                                        
                                    </c:choose>  
                                </form>    
                                
                                <form method="POST" id="subjectRenameForm${subject.id}" action="${pageContext.request.contextPath}/subjectRenameForm"> 
                                    <input type="hidden" name="subjectId" value="${subject.id}" />
                                    <div class="renameButton" id="renameButton${subject.id}">
                                        rename
                                    </div>
                                </form>
                                        
                                <form method="POST" id="subjectToGroup${subject.id}" action="${pageContext.request.contextPath}/subjectToGroup"> 
                                    <input type="hidden" name="subjectId" value="${subject.id}" />
                                    <input type="hidden" name="oldState" value="${subjectType}" />
                                    
                                    <c:choose>
                                        <c:when test="${subject.ifGroup eq true}">        
                                            <div class="subjectToGroupSelectButton" id="subjectToGroupButton${subject.id}">
                                                group
                                            </div>
                                        </c:when>

                                        <c:otherwise> 
                                            <div class="subjectToGroupButton" id="subjectToGroupButton${subject.id}">
                                                not group
                                            </div>
                                        </c:otherwise>
                                    </c:choose>
                                </form>
                            </td>

                        </tr>
                    </c:forEach>                                
                    
                </table>
            </c:otherwise>
        </c:choose>
        
    </c:otherwise>
</c:choose>