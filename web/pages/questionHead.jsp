<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
  

<c:choose>
    <c:when test="${ifNew ne 'true'}">       
        <a href = '${pageContext.request.contextPath}/subject/${subject.id}'><u>${subject.name}</u></a> - №${question.id}
    </c:when>

    <c:otherwise>
        New question        
    </c:otherwise>
</c:choose>