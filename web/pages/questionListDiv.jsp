<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
    

<c:choose>
    <c:when test="${questionsList eq null}">       
        <br>There are no questions :(
    </c:when>

    <c:otherwise>                
        
        <c:choose>
            <c:when test="${questionsListSize eq '0'}">       
                <br>There are no questions :(
            </c:when>

            <c:otherwise> 
                
                <c:forEach var="i" begin="0" end="4">
                    <ul class="questionsList">
                    
                        <c:forEach var="question" items="${questionsList}" varStatus="questionCount"> 

                            <c:if test="${(questionCount.count + (4 - i)) % 5 == 0}">        
                                <li>
                                    
                                    <c:choose>
                                        <c:when test="${question.state eq 'approved'}">        
                                            <a href = '${pageContext.request.contextPath}/question/${subject.id}/${question.id}'><u>${question.id}</u></a>
                                        </c:when>

                                        <c:when test="${question.state eq 'undefined'}">        
                                            <a href = '${pageContext.request.contextPath}/question/${subject.id}/${question.id}'><u><span class="yellowText">${question.id}</span></u></a>
                                        </c:when>        

                                        <c:otherwise>         
                                            <a href = '${pageContext.request.contextPath}/question/${subject.id}/${question.id}'><u><span class="greyText">${question.id}</span></u></a>
                                        </c:otherwise>
                                    </c:choose> 
                                    
                                    
                                    
                                </li>
                            </c:if>  

                        </c:forEach>

                    </ul>
                </c:forEach>

            </c:otherwise>
        </c:choose>
        
    </c:otherwise>
</c:choose>