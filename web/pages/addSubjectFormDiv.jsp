<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
  

<script> 
    jQuery(document).ready(function(){
    
        $(document).on('click', '#addSubjectSiteButton', function(){
            $("#addSubjectForm").submit();
        });
    });
</script>

<div class="addSubjectDiv">
    
    <div class="pageTitleDiv">
        Add subject: 
    </div>    
    
    <div class="addSubjectFormDiv">
        <form method="POST" action="${pageContext.request.contextPath}/addSubject" id="addSubjectForm">
            <table>
                <tr>
                    <td class="formText">
                        name
                    </td>
                    <td class="formInput">
                        <input type="text" name="subjectName" id="subjectName">           
                    </td>
                </tr>
            </table>
        </form> 
    </div>
    
    <div class="addSubjectButtonDiv" id="addSubjectSiteButton">        
        Add
    </div> 
           
</div>