<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>


<script>
    function getPasswordHash(password) {       
        return hex_md5(password);
    }
    
    jQuery(document).ready(function(){

        $(document).on('click', '#passwordChangeButton', function(){  
            
            if ($('#newPassword').val() !== '') {
                if ($('#newPassword').val() !== $('#newPasswordConfirmation').val()) {                              
                    $('#popupText').html("New password and new password confirmation don't match");
                    $('.popupShadow').show();
                } else {
                    $('#oldPasswordHidden').val(getPasswordHash($('#oldPassword').val()));
                    $('#newPasswordHidden').val(getPasswordHash($('#newPassword').val()));                    
                    $("#passwordChangeForm").submit();
                }
            } else {
                $('#popupText').html("New password must contain symbols");
                $('.popupShadow').show();
            }
        });        
    });
</script>

<div class="passwordChangeDiv">
    
    <div class="pageTitleDiv">
        Enter data to change password:
    </div>    
    
    <div class="passwordChangeFormDiv">
        <form method="POST" id="passwordChangeForm" action="${pageContext.request.contextPath}/passwordChange">
            <input type="hidden" name="newPassword" id="newPasswordHidden" value="" />
            <input type="hidden" name="oldPassword" id="oldPasswordHidden" value="" />
            <table>
                <tr>
                    <td class="formText">
                        Old password
                    </td>
                    <td class="formInput">
                        <input type="password" id="oldPassword"> 
                    </td>
                </tr>
                <tr>
                    <td class="formText">
                        New password
                    </td>
                    <td class="formInput">
                        <input type="password" id="newPassword">   
                    </td>
                </tr>
                <tr>
                    <td class="formText">
                        New password confirmation
                    </td>
                    <td class="formInput">
                        <input type="password" name="newPasswordConfirmation" id="newPasswordConfirmation">   
                    </td>
                </tr>
            </table>
        </form>
    </div>
    
    <div class="passwordChangeButtonDiv" id="passwordChangeButton">
        Change
    </div> 
</div>

