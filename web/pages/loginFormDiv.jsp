<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
 

<script> 
    function getPasswordHash(password) {       
        return hex_md5(password);
    }
    
    jQuery(document).ready(function(){
    
        $(document).on('click', '#enterToSiteButton', function(){
            $('#passwordHidden').val(getPasswordHash($('#password').val()));
            $("#enterToSiteForm").submit();
        });
    });
</script>

<div class="loginDiv">
    
    <div class="pageTitleDiv">
        Enter to site: 
    </div>    
    
    <div class="enterToSiteFormDiv">
        <form method="POST" action="${pageContext.request.contextPath}/login" id="enterToSiteForm">
            <input type="hidden" name="password" id="passwordHidden" value="" />
            <table>
                <tr>
                    <td class="formText">
                        login
                    </td>
                    <td class="formInput">
                        <input type="text" id="username" name="username" value="${username}" /> 
                    </td>
                </tr>
                <tr>
                    <td class="formText">
                        password
                    </td>
                    <td class="formInput">
                        <input type="password" id="password" />   
                    </td>
                </tr> 
                <tr>
                    <td class="formText">                        
                    </td>
                    <td>
                        <div class="formRememberCheckbox">
                            <input type="checkbox" name="remember" />
                        </div>
                        <div class="formRememberText">
                            remember me for 30 days
                        </div>
                    </td>
                </tr>  
            </table>
        </form>
    </div>   
                    
    <div class="passwordResetDiv">
        <a href="${pageContext.request.contextPath}/passwordResetForm"><u>reset password</u></a><br> 
    </div>
    
    <div class="enterToSiteButtonDiv" id="enterToSiteButton">
        Enter
    </div> 
</div>