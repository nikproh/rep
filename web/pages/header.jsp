<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<script>  
    
    jQuery(document).ready(function(){
        
        $("ul.dropdown li").hover(function(){
            $(this).addClass("hover");
            $('ul:first',this).css('visibility', 'visible');

        }, function(){
            $(this).removeClass("hover");
            $('ul:first',this).css('visibility', 'hidden');

        });

        $("ul.dropdown li ul li:has(ul)").find("a:first").append(" &raquo; ");
    });
</script>

<div class="headerDiv">
    <div class="headerImage">
        <img src="${pageContext.request.contextPath}/images/header.png"/>
    </div>

    <div class="accauntAction">
        <c:choose>
            <c:when test="${ifAuthtorize eq 'true'}">       
                <a href = '${pageContext.request.contextPath}/profile'><u>${pageContext.request.remoteUser}</u></a> • <a href = '${pageContext.request.contextPath}/logout'><u>logout</u></a>
            </c:when>                         
            <c:otherwise>                                       
                <a href = '${pageContext.request.contextPath}/loginForm'><u>login</u></a> • <a href = '${pageContext.request.contextPath}/registrationForm'><u>registration</u></a>
            </c:otherwise>
        </c:choose>
    </div>

    <div class="menuAction">    

        <ul class="dropdown">
            <li class="firstLevel">
                <a href = '${pageContext.request.contextPath}/allSubjects'>Subjects list</a>                
            </li>
            <li class="firstLevel">
                <a href = '${pageContext.request.contextPath}/question/new'>Add new question</a>                
            </li>
            <li class="firstLevel">
                <a href = '${pageContext.request.contextPath}/userQuestions'>My questions</a>        
            </li>

            <c:if test="${ifAdmin eq 'true'}">  

                <c:choose>
                    <c:when test="${undefinedQuestionsSize eq 0}">                                          
                        <li class="firstLevel"><a class="notLink">Question</a>
                    </c:when>                         
                    <c:otherwise>                                       
                        <li class="firstLevel"><a class="notLink">Question (${undefinedQuestionsSize})</a>
                    </c:otherwise>
                </c:choose> 
                
                    <ul class="innerList">
                        <li class="secondLevel">
                            <a href = '${pageContext.request.contextPath}/undefinedQuestions'>
                                <c:choose>
                                    <c:when test="${undefinedQuestionsSize eq 0}">                                          
                                        Undefined 
                                    </c:when>                         
                                    <c:otherwise>                                       
                                        Undefined (${undefinedQuestionsSize})
                                    </c:otherwise>
                                </c:choose> 
                            </a>
                        </li>        			 
                        <li class="secondLevel">
                            <a href = '${pageContext.request.contextPath}/approvedQuestions'>Approved</a>
                        </li>
                        <li class="secondLevel">
                            <a href = '${pageContext.request.contextPath}/discardQuestions'>Discard</a>
                        </li>
                    </ul>
                </li>
                
                <c:choose>
                    <c:when test="${undefinedSubjectsSize eq 0}">                                          
                        <li class="firstLevel"><a class="notLink">Subject</a>
                    </c:when>                         
                    <c:otherwise>                                       
                        <li class="firstLevel"><a class="notLink">Subject (${undefinedSubjectsSize})</a>
                    </c:otherwise>
                </c:choose> 
                    <ul class="innerList">
                        <li class="secondLevel">
                            <a href = '${pageContext.request.contextPath}/undefinedSubjects'>
                                <c:choose>
                                    <c:when test="${undefinedSubjectsSize eq 0}">                                          
                                        Undefined 
                                    </c:when>                         
                                    <c:otherwise>                                       
                                        Undefined (${undefinedSubjectsSize})
                                    </c:otherwise>
                                </c:choose> 
                            </a>
                        </li>        			 
                        <li class="secondLevel">
                            <a href = '${pageContext.request.contextPath}/approvedSubjects'>Approved</a>
                        </li>
                        <li class="secondLevel">
                            <a href = '${pageContext.request.contextPath}/discardSubjects'>Discard</a>
                        </li>
                    </ul>
                </li>                
                <li class="firstLevel">
                    <a href = '${pageContext.request.contextPath}/newComments'>                        
                        <c:choose>
                            <c:when test="${newComments eq 0}">                                          
                                New comments
                            </c:when>                         
                            <c:otherwise>                                       
                                New comments (${newComments})
                            </c:otherwise>
                        </c:choose>
                    </a>                
                </li>
                <li class="firstLevel"><a class="notLink">Different</a>
                    <ul class="innerList">                     			 
                        <li class="secondLevel">
                            <a href = '${pageContext.request.contextPath}/allUsers'>Users</a>
                        </li>
                        <li class="secondLevel">
                            <a href = '${pageContext.request.contextPath}/addSubjectForm'>Add subject</a>
                        </li>
                    </ul>
                </li>
            </c:if> 
        </ul>  
    </div>
</div>
        
