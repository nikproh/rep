<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
    

<script>        

    jQuery(document).ready(function(){

        $(document).on('click', '.popupButton', function(){
            
            $('.popupShadow').hide();
        });

    });

</script>    

<c:choose>
    <c:when test="${popupWindow eq 'true'}"> 
        <div class="popupShadow">
            not hidden
    </c:when>

    <c:otherwise> 
        <div class="popupShadow" hidden="true">
    </c:otherwise>
</c:choose>
    
    <div class="popupWindow">
        <div class="popupImage">
            <c:choose>
                <c:when test="${popupGood eq 'true'}">       
                    <img src="${pageContext.request.contextPath}/images/success.png"/>
                </c:when>

                <c:otherwise> 
                    <img src="${pageContext.request.contextPath}/images/fail.png"/>
                </c:otherwise>
            </c:choose>
        </div>

        <div class="popupText" id="popupText">
            ${popupText}
        </div>

        <div class="popupButton">
            OK
        </div>
    </div>
</div>   