<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>


<div class="allGroupsDiv">
    
    <div class="pageTitleDiv">
        Groups list:
    </div>

    <c:choose>
        <c:when test="${allGroupsSize eq '0'}">
            <br>There are no groups :(
        </c:when>

        <c:otherwise>         
            <div class="subjectsDiv">
                
                <c:forEach var="i" begin="0" end="3">
                    <ul class="subjectsList">            

                        <c:if test="${allGroups ne null}">  
                            <c:if test="${allGroupsSize ne '0'}">

                                <c:forEach var="subject" items="${allGroups}" varStatus="subjectCount"> 

                                    <c:if test="${(subjectCount.count + (3 - i)) % 4 == 0}">        
                                        <li>
                                            <a href = '${pageContext.request.contextPath}/subject/${subject.id}'><u>${subject.name} (${allGroupsLen[subjectCount.count - 1]})</u></a>
                                        </li>
                                    </c:if>  

                                </c:forEach>

                            </c:if>            
                        </c:if>                    
                    </ul>                    
                </c:forEach>
                                
            </div>
            
            <div style="clear: both;"></div> 
        </c:otherwise>
    </c:choose>
</div>

<div class="allSubjectsDiv">
    
    <div class="pageTitleDiv">
        Subjects list:
    </div>

    <c:choose>
        <c:when test="${allSubjectsSize eq '0'}">
            <br>There are no subjects :(
        </c:when>

        <c:otherwise>         
            <div class="subjectsDiv">
                
                <c:forEach var="i" begin="0" end="3">
                    <ul class="subjectsList">            

                        <c:if test="${allSubjects ne null}">  
                            <c:if test="${allSubjectsSize ne '0'}">

                                <c:forEach var="subject" items="${allSubjects}" varStatus="subjectCount"> 

                                    <c:if test="${(subjectCount.count + (3 - i)) % 4 == 0}">        
                                        <li>
                                            <a href = '${pageContext.request.contextPath}/subject/${subject.id}'><u>${subject.name} (${allSubjectsLen[subjectCount.count - 1]})</u></a>
                                        </li>
                                    </c:if>  

                                </c:forEach>

                            </c:if>            
                        </c:if>                    
                    </ul>                    
                </c:forEach>
                                
            </div>
            
            <div style="clear: both;"></div> 
        </c:otherwise>
    </c:choose>
</div>