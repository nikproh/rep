<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
    

<div class="questionListDiv">
    
    <div class="pageTitleDiv">
        Approved subjects:
    </div>
    
    <div class="questionListInnerDiv">
        <%@include file="subjectsListDiv.jsp" %> 
    </div>
         
</div>