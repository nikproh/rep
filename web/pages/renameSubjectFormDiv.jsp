<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
  

<script> 
    jQuery(document).ready(function(){
    
        $(document).on('click', '#subjectRenameButton', function(){
            $("#subjectRenameForm").submit();
        });
    });
</script>

<div class="addSubjectDiv">
    
    <div class="pageTitleDiv">
        Rename subject (${subject.name}): 
    </div>    
    
    <div class="addSubjectFormDiv">        
        <form method="POST" action="${pageContext.request.contextPath}/subjectRename" id="subjectRenameForm">
            <input type="hidden" name="subjectId" value="${subject.id}" />
            <table>
                <tr>
                    <td class="formText">
                        new name
                    </td>
                    <td class="formInput">
                        <input type="text" name="subjectName" id="subjectName">           
                    </td>
                </tr>
            </table>
        </form> 
    </div>
    
    <div class="subjectRenameButtonDiv" id="subjectRenameButton">        
        Rename
    </div> 
           
</div>