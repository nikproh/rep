<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>


<script>
    jQuery(document).ready(function(){

        $(document).on('click', '#passwordResetButton', function(){  

            $("#passwordResetForm").submit();
            
        });        
    });
</script>

<div class="passwordResetPageDiv">
    
    <div class="pageTitleDiv">
        Enter login to reset password:
    </div>    
    
    <div class="passwordResetFormPageDiv">
        <form method="POST" id="passwordResetForm" action="${pageContext.request.contextPath}/passwordReset">
            <table>
                <tr>
                    <td class="formText">
                        Login
                    </td>
                    <td class="formInput">
                        <input type="text" name="login" id="username"> 
                    </td>
                </tr>
            </table>        
        </form>
    </div>
    
    <div class="passwordResetButtonPageDiv" id="passwordResetButton">
        Reset
    </div> 
</div>




