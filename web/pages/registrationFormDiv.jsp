<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
 

<script>
    function validateEmail(email) {
        var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        return re.test(email);
    }
    
    function getPasswordHash(password) {       
        return hex_md5(password);
    }
    
    jQuery(document).ready(function(){

        $('#registrationButton').click(function(){  
            
            var result = '';
            
            if (!validateEmail($('#username').val())) {
                result = "Something wrong with email";
            } else {
                if ($('#password').val() === '') {
                    result = "Password can't be empty";
                } else {                    
                    if ($('#password').val() !== $('#passwordConfirmation').val()) {
                        result = "Password and password confirmation don't match";
                    }
                }
            }

            if (result !== '') {               
                $('#popupText').html(result);
                $('.popupShadow').show();
            } else {
                $('#passwordHidden').val(getPasswordHash($('#password').val()));
                $("#registrationForm").submit();
            }
        });
        
    });
</script>

<div class="registrationDiv">
    
    <div class="pageTitleDiv">
        Registration:
    </div>

    <div class="registrationFormDiv">   
        <form method="POST" id="registrationForm" action="${pageContext.request.contextPath}/registration">   
            <input type="hidden" name="password" id="passwordHidden" value="" />
            <table>
                <tr>
                    <td class="formText">
                        Email
                    </td>
                    <td class="formInput">
                        <input type="text" name="username" id="username" value="${username}" />
                    </td>
                </tr>
                <tr>
                    <td class="formText">
                        Password
                    </td>
                    <td class="formInput">
                        <input type="password" id="password" />       
                    </td>
                </tr>    
                <tr>
                    <td class="formText">
                        Password confirmation
                    </td>
                    <td class="formInput">
                        <input type="password" name="passwordConfirmation" id="passwordConfirmation">   
                    </td>
                </tr>
                <tr>
                    <td class="formText">
                        Name
                    </td>
                    <td class="formInput">
                        <input type="text" name="name" id="name" value="${name}" />
                    </td>
                </tr> 
            </table>    
        </form>  
    </div>  
    
    <div class="registrationButtonDiv" id="registrationButton">
        Registration
    </div> 
</div>