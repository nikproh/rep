<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="t" tagdir="/WEB-INF/tags" %>


<t:headerBodyFooter>
    
    <jsp:attribute name="title">    
        Registration
    </jsp:attribute>
    
    <jsp:attribute name="header">    
        <jsp:include page="/header" />
    </jsp:attribute>
    
    <jsp:attribute name="content">
        <%@include file="registrationFormDiv.jsp" %> 
    </jsp:attribute>
    
    <jsp:attribute name="footer">
        <%@include file="footer.jsp" %> 
    </jsp:attribute>
        
    <jsp:body>        
    </jsp:body>
        
</t:headerBodyFooter>