<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>


<div class="questionListDiv">
    
    <div class="pageTitleDiv">
        My questions:
    </div>
    
    <div class="questionListInnerUserListDiv">  
        
        <c:choose>
            <c:when test="${questionsListSize eq 0}">       
                <br>There are no questions :(
            </c:when>

            <c:otherwise>   
        
                <table class="subjectTableDiv">
                    <c:forEach var="question" items="${questionsList}"> 
                        <tr>

                            <td class="subjectTableLeftColumn">
                                <a href = '${pageContext.request.contextPath}/question/${subject.id}/${question.id}'><u>${question.id}</u></a>
                            </td>

                            <td class="questionTableRightColumn">
                                ${question.state}
                            </td>

                            <td class="questionTableLastColumn">

                                <c:forEach var="subject" items="${question.subjects}" begin="0" end="2"> 
                                    <a href = '${pageContext.request.contextPath}/subject/${subject.id}'><u>${subject.name}</u></a>&nbsp;&nbsp;&nbsp;
                                </c:forEach>

                                <c:forEach var="subject" items="${question.subjects}" begin="3" end="3"> 
                                    ...
                                </c:forEach>    
                            </td>
                        </tr>
                    </c:forEach>    
                </table>
            </c:otherwise>
        </c:choose>
    </div>
         
</div>